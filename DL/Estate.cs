﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Emap.Web.DL
{
    public class Estate
    {
        public int Id { get; set; }
        public string ImageUrl { get; set; }
        public string Name { get; set; }
        public string Area { get; set; }
        public string Rent { get; set; }
        public string Highway { get; set; }
        public string Airport { get; set; }
        public string Port { get; set; }
        public string distance_to_airport { get; set; }
        public string distance_to_port { get; set; }
        public string Electricity { get; set; }
        public string Gas { get; set; }
        public string Water { get; set; }
        public string SectorFocus { get; set; }
        public string Address { get; set; }
        public string Land { get; set; }
        public string ActiveFirms { get; set; }
        public string UpcomingFirms { get; set; }
        public string TotalInvestment { get; set; }
        public string ForeignInvestment { get; set; }
        public string Employed { get; set; }
        public string Omanisation { get; set; }
        public string TopActivities { get; set; }
    }
}