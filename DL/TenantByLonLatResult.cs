﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Emap.Web.DL
{
    public class TenantByLonLatResult
    {
        public int fid { get; set; }
        public string wkt { get; set; }
        public string Landuse { get; set; }
        public string Pnu { get; set; }
        public double Area { get; set; }
        public bool Invest { get; set; }

        public List<Tenant> Tenants { get; set; }

        public TenantByLonLatResult()
        {
            Tenants = new List<Tenant>();
        }
    }
}