﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Emap.Web.DL
{
    public class Legend
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Color { get; set; }
    }
}