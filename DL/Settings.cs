﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Emap.Web.DL
{
    public class Settings
    {

        public string welcomeText { get; set; }
        public string tabTitle_1 { get; set; }
        public string tabContent_1 { get; set; }
        public string tabTitle_2 { get; set; }
        public string tabContent_2 { get; set; }
        public string tabTitle_3 { get; set; }
        public string tabContent_3 { get; set; }
        public string tabTitle_4 { get; set; }
        public string tabContent_4 { get; set; }
        public string tabTitle_5 { get; set; }
        public string tabContent_5 { get; set; }
        public string help { get; set; }
    }
}