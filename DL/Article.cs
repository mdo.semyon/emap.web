﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Emap.Web.DL
{
    public class Article
    {
        public bool IsEstate { get; set; }
        public string formatted_address { get; set; }
        public string EstateName { get; set; }
        public int Id { get; set; }
        public int EstateId { get; set; }
        public double lon { get; set; }
        public double lat { get; set; }
    }
}