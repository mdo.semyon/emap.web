﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Emap.Web.DL
{
    public class Tenant
    {
        public int? Id { get; set; }
        public int EstateId { get; set; }
        public string Name { get; set; }
        public string EstateName { get; set; }
        public string wkt { get; set; }
        public int Count { get; set; }
        public string Landuse { get; set; }
        public string Pnu { get; set; }
    }
}