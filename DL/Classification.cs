﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Emap.Web.DL
{
    public class Classification
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}