﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Emap.Web.DL
{
    public class Place
    {
        public string formatted_address { get; set; }
        public double lon { get; set; }
        public double lat { get; set; }
    }
}