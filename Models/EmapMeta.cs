﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Emap.Web.Models
{
    public class EmapMeta
    {
        public Settings Settings { get; set; }
        public Meta Meta { get; set; }
        public Facebook Facebook { get; set; }
        public Twitter Twitter { get; set; }
    }
    public class Settings
    {
        public string liveChatLicense { get; set; }
        public string workspace { get; set; }
        public string wmsURL { get; set; }
        public string bingKey { get; set; }
        public string companyProfileURL  { get; set; }
        public string map_mode { get; set; }
        public string map_type { get; set; }

        public Settings()
        {
        }
    }

    public class Meta
    {
        public string en_title { get; set; }
        public string en_description { get; set; }
        public string en_keywords { get; set; }
        public string en_twitter_message { get; set; }
        public string ar_title { get; set; }
        public string ar_description { get; set; }
        public string ar_keywords { get; set; }
        public string ar_twitter_message { get; set; }

        public Meta()
        {
        }
    }
    public class Facebook
    {
        public string en_name { get; set; }
        public string en_description { get; set; }
        public string ar_name { get; set; }
        public string ar_description { get; set; }
        public string picture { get; set; }

        public Facebook()
        {
        }
    }
    public class Twitter
    {
        public string en_text { get; set; }
        public string ar_text { get; set; }

        public Twitter()
        {
        }
    }
}