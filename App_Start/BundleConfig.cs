﻿using System.Web;
using System.Web.Optimization;

namespace Emap.Web
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Javascript
            //main
            bundles.Add(new ScriptBundle("~/bundles/libs").Include(
                        "~/Scripts/libs/base/jquery.js",
                        "~/Scripts/libs/ol/ol.js",
                        "~/Scripts/libs/ol/common.js",
                        "~/Scripts/libs/angular/angular.js",
                        "~/Scripts/libs/angular/angular-sanitize.min.js",
                        "~/Scripts/libs/angular/autocomplete.js",
                        "~/Scripts/libs/angular/angular-animate.js",
                        "~/Scripts/libs/angular/angular-busy.js",
                        "~/Scripts/libs/base/angulike.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                        "~/Scripts/emap.js",
                        "~/Scripts/constants.js",
                        "~/Scripts/map.js",
                        "~/Scripts/search.js",
                        "~/Scripts/mainController.js"));

            /*
            //start
            bundles.Add(new ScriptBundle("~/bundles/start_libs").Include(
                        "~/Scripts/libs/base/jquery.js",
                        "~/Scripts/libs/ol/ol.js",
                        "~/Scripts/libs/ol/common.js",
                        "~/Scripts/libs/angular/angular.js",
                        "~/Scripts/libs/angular/angular-sanitize.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/start").Include(
                        "~/Scripts/constants.js",
                        "~/Scripts/map.js",
                        "~/Scripts/startSearch.js",
                        "~/Scripts/startController.js"));
             */
            
            //en
            bundles.Add(new ScriptBundle("~/bundles/bootstrap_en").Include("~/Scripts/libs/base/bootstrap.js"));
            //ar
            //bundles.Add(new ScriptBundle("~/bundles/bootstrap_ar").Include("~/Scripts/libs/base/bootstrap-arabic.js"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap_ar").Include("~/Scripts/libs/base/bootstrap.js"));
            

            //CSS
            //main
            bundles.Add(new StyleBundle("~/Content/libs").Include(
                        "~/Content/themes/base/font-awesome.css",
                        "~/Content/themes/base/whhg.css",
                        "~/Content/themes/ol/ol.css",
                        "~/Content/themes/ol/ol3gm.css",
                        "~/Content/themes/ol/ol-popup.css",
                        "~/Content/themes/angular/autocomplete.css",
                        "~/Content/themes/angular/angular-busy.css"));


            /*
            //start
            bundles.Add(new StyleBundle("~/Content/start_libs").Include(
                        "~/Content/themes/base/font-awesome.css",
                        "~/Content/themes/base/whhg.css",
                        "~/Content/themes/ol/ol.css",
                        "~/Content/themes/ol/ol-popup.css"));
            */ 

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/less/Site.css"));

            //en
            bundles.Add(new StyleBundle("~/Content/bootstrap_en").Include("~/Content/themes/base/bootstrap.css"));
            bundles.Add(new StyleBundle("~/Content/site_en").Include("~/Content/less/Site_en.css"));
            //ar
            bundles.Add(new StyleBundle("~/Content/bootstrap_ar").Include("~/Content/themes/base/bootstrap-arabic.css"));
            bundles.Add(new StyleBundle("~/Content/site_ar").Include("~/Content/less/Site_ar.css"));

            BundleTable.EnableOptimizations = true;
        }
    }
}