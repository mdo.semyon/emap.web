﻿using Emap.Web.DL;
using EmapEstateBuilder;
using Newtonsoft.Json;
using Resources;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Script.Services;
using System.Web.Services;

namespace Emap.Web
{
    /// <summary>
    /// Summary description for EmapWebService
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    [System.Web.Script.Services.ScriptService]
    public class EmapWebService : System.Web.Services.WebService
    {
        public string connectionstring = WebConfigurationManager.AppSettings["ConnectionString"].ToString();
        public string GlobalTable = WebConfigurationManager.AppSettings["GlobalTable"].ToString();
        public string SettingsTable = WebConfigurationManager.AppSettings["SettingsTable"].ToString();
        public string EstatesDataTable = WebConfigurationManager.AppSettings["EstatesDataTable"].ToString();
        public string ClassificationTable = WebConfigurationManager.AppSettings["ClassificationTable"].ToString();
        public string ClassificationItemsTable = WebConfigurationManager.AppSettings["ClassificationItemsTable"].ToString();

        public int estate = 0;

        [WebMethod]
        public string getArticlesBySearchString(string searchString, string prefix = "en")
        {
            List<Article> res = new List<Article>();

            using (var conn = new SqlConnection(connectionstring))
            using (var command = new SqlCommand(prefix + "_getPredictions", conn) { 
                CommandType = CommandType.StoredProcedure
            })
            {
                command.Parameters.Add("query", SqlDbType.NVarChar).Value = searchString;
                command.Parameters.Add("estateId", SqlDbType.Int).Value = DBNull.Value;

                conn.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    res.Add(new Article
                    {
                        Id = reader.GetValue<int>("Id"),
                        EstateId = reader.GetValue<int>("EstateId"),
                        formatted_address = reader.GetValue<string>("Name"),
                        EstateName = reader.GetValue<string>("EstateName")
                    });

                }
                conn.Close();
            }

            return JsonConvert.SerializeObject(res);
        }
        [WebMethod]
        public string getArticlesBySearchStringAndEstateId(string searchString, int estateId, string prefix = "en")
        {
            List<Article> res = new List<Article>();

            using (var conn = new SqlConnection(connectionstring))
            using (var command = new SqlCommand(prefix + "_getPredictions", conn)
            {
                CommandType = CommandType.StoredProcedure
            })
            {
                command.Parameters.Add("query", SqlDbType.NVarChar).Value = searchString;
                command.Parameters.Add("estateId", SqlDbType.Int).Value = estateId;

                conn.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    res.Add(new Article
                    {
                        Id = reader.GetValue<int>("Id"),
                        EstateId = reader.GetValue<int>("EstateId"),
                        formatted_address = reader.GetValue<string>("Name"),
                        EstateName = reader.GetValue<string>("EstateName")
                    });

                }
                conn.Close();
            }

            return JsonConvert.SerializeObject(res);
        }
        [WebMethod]
        public string getCountriesBySearchStringAndEstateId(string searchString, int estateId, string prefix = "en")
        {
            List<Article> res = new List<Article>();

            using (var conn = new SqlConnection(connectionstring))
            using (var command = new SqlCommand(prefix + "_getCountryPredictions", conn)
            {
                CommandType = CommandType.StoredProcedure
            })
            {
                command.Parameters.Add("query", SqlDbType.NVarChar).Value = searchString;
                command.Parameters.Add("estateId", SqlDbType.Int).Value = estateId;

                conn.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    res.Add(new Article
                    {
                        EstateId = reader.GetValue<int>("EstateId"),
                        formatted_address = reader.GetValue<string>("Name")
                    });

                }
                conn.Close();
            }

            return JsonConvert.SerializeObject(res);
        }
        [WebMethod]
        public string getProductTypesBySearchStringAndEstateId(string searchString, int estateId, string prefix = "en")
        {
            List<Article> res = new List<Article>();

            using (var conn = new SqlConnection(connectionstring))
            using (var command = new SqlCommand(prefix + "_getProductTypePredictions", conn)
            {
                CommandType = CommandType.StoredProcedure
            })
            {
                command.Parameters.Add("query", SqlDbType.NVarChar).Value = searchString;
                command.Parameters.Add("estateId", SqlDbType.Int).Value = estateId;

                conn.Open();
                var reader = command.ExecuteReader();
                while (reader.Read())
                {
                    res.Add(new Article
                    {
                        EstateId = reader.GetValue<int>("EstateId"),
                        formatted_address = reader.GetValue<string>("Name")
                    });

                }
                conn.Close();
            }

            return JsonConvert.SerializeObject(res);
        }
        [WebMethod]
        public string getEstates(string prefix = "en")
        {
            List<Article> res = new List<Article>();

            var sqlstring = "SELECT DISTINCT " +
                            "Estate_Code AS Id, " +
                            "Estate_Code AS EstateId, " +
                            "LTRIM(RTRIM(location_" + prefix + ")) AS Name, " +
                            "LTRIM(RTRIM(location_" + prefix + ")) AS EstateName, " +
                            "cast(1 as bit) AS IsEstate, " +
                            "longitude, " +
                            "latitude " +
                            "FROM " + EstatesDataTable +" " +
                            "WHERE location_" + prefix + " IS NOT NULL ";

            SqlDataAdapter da = new SqlDataAdapter(sqlstring, connectionstring);
            DataSet ds = new DataSet();
            da.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                res.Add(new Article
                {
                    Id = (int)r["Id"],
                    EstateId = (int)r["EstateId"],
                    formatted_address = r["Name"].ToString(),
                    EstateName = r["EstateName"].ToString(),
                    IsEstate = (bool)r["IsEstate"],
                    lon = (double)r["longitude"],
                    lat = (double)r["latitude"]
                });
            }

            return JsonConvert.SerializeObject(res);
        }
        [WebMethod]
        public string getClassifications(string prefix = "en")
        {
            List<Classification> res = new List<Classification>();

            var sqlstring = "SELECT id, " + prefix + "_Name AS Name FROM " + ClassificationTable + " ORDER BY position";

            SqlDataAdapter da = new SqlDataAdapter(sqlstring, connectionstring);
            DataSet ds = new DataSet();
            da.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                res.Add(new Classification
                {
                    Id = (int)r["id"],
                    Name = r["Name"].ToString()
                });
            }

            return JsonConvert.SerializeObject(res);
        }
        [WebMethod]
        public string getLegends(int classId, string prefix = "en")
        {
            List<Legend> res = new List<Legend>();

            var sqlstring = "SELECT id, " + prefix + "_Name AS Name, Color FROM " + ClassificationItemsTable + " WHERE classID = " + classId;

            SqlDataAdapter da = new SqlDataAdapter(sqlstring, connectionstring);
            DataSet ds = new DataSet();
            da.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                res.Add(new Legend
                {
                    Id = (int)r["id"],
                    Name = r["Name"].ToString(),
                    Color = r["Color"].ToString()
                });
            }

            return JsonConvert.SerializeObject(res);
        }
        [WebMethod]
        public string getTenantsByEstateId(int estateId, string prefix = "en")
        {
            prefix = (prefix == "ar") ? "a" : "e";

            List<Tenant> res = new List<Tenant>();

            var sqlstring = "SELECT DISTINCT tid, tname_" + prefix + " FROM " + GlobalTable +" WHERE tid is not null AND estateid = " + estateId + " ORDER BY tname_" + prefix + ";";

            SqlDataAdapter da = new SqlDataAdapter(sqlstring, connectionstring);
            DataSet ds = new DataSet();
            da.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows) {
                res.Add(new Tenant
                {
                    Id = (int)r["tid"],
                    Name = r["tname_" + prefix + ""].ToString()
                });
            }

            return JsonConvert.SerializeObject(res);
        }
        
        [WebMethod]
        public string getTenantByLonLat(double lon, double lat, int estateId, string prefix = "en")
        {
            var prefix_old = (prefix == "ar") ? "a" : "e";

            int SRID = (estateId == 3 || estateId == 8 || estateId == 10) ? 32640 : (estateId == 4 || estateId == 9) ? 32639 : 23240;

            TenantByLonLatResult res = new TenantByLonLatResult();

            //parsels
            var sqlstring = "SELECT TOP 1 geom.STAsText() as wkt, geom.MakeValid().STArea() as area, tid, " +
                            "Landuse_e, " +
                            "Landuse_" + prefix_old + " AS Landuse, " +
                            "pnu," +
                            "Invest," +
                            "fid " +
                            "FROM " + GlobalTable +" " +
                            "WHERE estateid = " + estateId + " AND geom.MakeValid().STIntersects(geometry::STGeomFromText('POINT(" + lon.ToString().Replace(',', '.') + " " + lat.ToString().Replace(',', '.') + ")', " + SRID + ")) = 1 ";
                            
            SqlDataAdapter da = new SqlDataAdapter(sqlstring, connectionstring);
            DataSet ds = new DataSet();
            da.Fill(ds);


            string temp = string.Empty;
            int? tid = null;
            foreach (DataRow r in ds.Tables[0].Rows)
            {
                temp = "POLYGON ((";
                temp += convert(r.GetValue<string>("wkt"), estateId);
                temp += "))";
                res.wkt = temp;
                res.fid = r.GetValue<int>("fid");

                tid = r.GetValue<int?>("tid");
                if (!tid.HasValue)
                {
                    res.fid = r.GetValue<int>("fid");
                    res.Area = r.GetValue<double>("area");
                    res.Pnu = r.GetValue<string>("pnu");
                    res.Landuse = r.GetValue<string>("Landuse");
                    res.Invest = r.GetValue<bool>("Invest");

                    var Landuse_e = r.GetValue<string>("Landuse_e");
                    if (Landuse_e == "Governmental" || Landuse_e == "Residental")
                        res.Landuse = string.Empty;
                }

                break;
            }

            //details
            if (tid.HasValue)
            {
                sqlstring = "SELECT count(distinct pnu) cnt, tid, " +
                            "tname_" + prefix_old + " AS Name, " +
                            "Landuse_" + prefix_old + " AS Landuse " +
                            "FROM " + GlobalTable +" " +
                            "WHERE tid in (select distinct tid from " + GlobalTable +" where estateid = " + estateId + " AND geom.MakeValid().STIntersects(geometry::STGeomFromText('POINT(" + lon.ToString().Replace(',', '.') + " " + lat.ToString().Replace(',', '.') + ")', " + SRID + ")) = 1) " +
                            "GROUP BY tid, Landuse_" + prefix_old + ", tname_" + prefix_old;

                da = new SqlDataAdapter(sqlstring, connectionstring);
                ds = new DataSet();
                da.Fill(ds);

                foreach (DataRow r in ds.Tables[0].Rows)
                {
                    res.Tenants.Add(new Tenant
                    {
                        Count = r.GetValue<int>("cnt"),
                        Id = r.GetValue<int?>("tid"),
                        Name = r.GetValue<string>("Name"),
                        Landuse = r.GetValue<string>("Landuse")
                    });
                }

            }

            return JsonConvert.SerializeObject(res);
        }
        
        [WebMethod]
        public string getProductTypes(int estateId, string prefix = "en")
        {
            prefix = (prefix == "ar") ? "a" : "e";

            List<ProductType> res = new List<ProductType>();

            var sqlstring = "SELECT DISTINCT prodtype_" + prefix + " " +
                            "FROM " + GlobalTable +" " +
                            "WHERE estateid = " + estateId + " AND prodtype_" + prefix + " IS NOT NULL ORDER BY prodtype_" + prefix + "";

            SqlDataAdapter da = new SqlDataAdapter(sqlstring, connectionstring);
            DataSet ds = new DataSet();
            da.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                res.Add(new ProductType
                {
                    Name = r["prodtype_" + prefix + ""].ToString()
                });
            }
            return JsonConvert.SerializeObject(res);
        }
        [WebMethod]
        public string getSubCatTenants(int estateId, string productType, string prefix = "en")
        {
            prefix = (prefix == "ar") ? "a" : "e";

            List<Tenant> res = new List<Tenant>();

            var sqlstring = "SELECT DISTINCT estateid, tid, tname_" + prefix + " " +
                            "FROM " + GlobalTable +" " +
                            "WHERE prodtype_" + prefix + " = '" + productType + "' AND estateid = " + estateId;

            SqlDataAdapter da = new SqlDataAdapter(sqlstring, connectionstring);
            DataSet ds = new DataSet();
            da.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                res.Add(new Tenant
                {
                    Id = (int)r["tid"],
                    Name = r["tname_" + prefix + ""].ToString(),
                    EstateId = (int)r["estateid"]
                });
            }

            return JsonConvert.SerializeObject(res);
        }
        [WebMethod]
        public string getInvestmets(int estateId, string prefix = "en")
        {
            prefix = (prefix == "ar") ? "a" : "e";

            List<Country> res = new List<Country>();

            var sqlstring = "SELECT distinct cnationality_" + prefix + " " +
                            "FROM  " + GlobalTable +"  WHERE estateid=" + estateId + " AND cnationality_" + prefix + " IS NOT NULL order by cnationality_" + prefix + "";

            SqlDataAdapter da = new SqlDataAdapter(sqlstring, connectionstring);
            DataSet ds = new DataSet();
            da.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                res.Add(new Country
                {
                    Name = r["cnationality_" + prefix + ""].ToString()
                });
            }
            return JsonConvert.SerializeObject(res);
        }
        [WebMethod]
        public string getSubCatInvestments(int estateId, string investmentsOrigin, string prefix = "en")
        {
            prefix = (prefix == "ar") ? "a" : "e";

            List<Tenant> res = new List<Tenant>();

            var sqlstring = "SELECT DISTINCT estateid, tid, tname_" + prefix + " " +
                            "FROM " + GlobalTable +" " +
                            "WHERE cnationality_" + prefix + " = '" + investmentsOrigin + "' AND estateid = " + estateId;

            SqlDataAdapter da = new SqlDataAdapter(sqlstring, connectionstring);
            DataSet ds = new DataSet();
            da.Fill(ds);

            foreach (DataRow r in ds.Tables[0].Rows)
            {
                res.Add(new Tenant
                {
                    Id = (int)r["tid"],
                    Name = r["tname_" + prefix + ""].ToString(),
                    EstateId = (int)r["estateid"]
                });
            }

            return JsonConvert.SerializeObject(res);
        }
        [WebMethod]
        public string getEsateDetails(int estateId, string prefix = "en") {

            var activities_prefix = (prefix == "ar") ? "_a" : string.Empty;

            var sqlstring = "SELECT TOP 1 "
                + "d.Estate_Code AS estateId"
                + ",d.estate_image"
                + ",d.location_" + prefix + " AS name" 
                + ",d.Total_Area AS area"
                + ",d.rent_policy_" + prefix + " AS rent"
                + ",d.nearest_major_highway_" + prefix + " AS highway"
                + ",d.nearest_airport_" + prefix + " AS airport"
                + ",d.distance_to_airport"
                + ",d.nearest_port_facility_" + prefix + " AS port"
                + ",d.distance_to_port"
                + ",d.electricity_supplier_" + prefix + " AS electricity"
                + ",d.gas_supplier_" + prefix + " AS gas"
                + ",d.water_source_" + prefix + " AS water"
                + ",d.sector_focus_" + prefix + " AS sectorFocus"
                + ",d.address_" + prefix + " AS address"
                + ",d.available_land AS land"
                + ",d.firms_in_production AS activeFirms"
                + ",d.firms_in_other_stages AS upcomingFirms"
                + ",d.total_investment AS totalInvestment"
                + ",d.foreign_invetment AS foreignInvestment"
                + ",d.no_of_employees AS employed"
                + ",d.Omanization AS omanisation"
                + ",d.main_industrial_activities" + activities_prefix + " AS topActivities"
                + " FROM " + EstatesDataTable +" AS d "
                + "WHERE d.Estate_Code = " + estateId;

            SqlDataAdapter da = new SqlDataAdapter(sqlstring, connectionstring);
            DataSet ds = new DataSet();
            da.Fill(ds);

            var r = ds.Tables[0].Rows[0];
            Estate res = new Estate {
                ImageUrl = r["estate_image"].ToString(),
                Name = r["name"].ToString(),
                Area = r["area"].ToString(),
                Rent = r["rent"].ToString(),
                Highway = r["highway"].ToString(),
                Airport = r["airport"].ToString(),
                Port = r["port"].ToString(),
                distance_to_airport = r["distance_to_airport"].ToString(),
                distance_to_port = r["distance_to_port"].ToString(),
                Electricity = r["electricity"].ToString(),
                Gas = r["gas"].ToString(),
                Water = r["water"].ToString(),
                SectorFocus = r["sectorFocus"].ToString(),
                Address = r["address"].ToString(),
                Land = r["land"].ToString(),
                ActiveFirms = r["activeFirms"].ToString(),
                UpcomingFirms = r["upcomingFirms"].ToString(),
                TotalInvestment = r["totalInvestment"].ToString(),
                ForeignInvestment = r["foreignInvestment"].ToString(),
                Employed = r["employed"].ToString(),
                Omanisation = r["omanisation"].ToString(),
                TopActivities = r["topActivities"].ToString()
            };
            return JsonConvert.SerializeObject(res);
        }
        /*
        [WebMethod]
        public string getFromRouteList(string searchString, string prefix = "en")
        {
            List<Place> res = new List<Place>();

            string sqlstring = "SELECT fromRoute, latlong FROM " + RouteTable + " WHERE fromRoute IS NOT NULL AND LOWER(fromRoute) LIKE LOWER('" + searchString + "%')";

            SqlDataAdapter da = new SqlDataAdapter(sqlstring, connectionstring);
            DataSet ds = new DataSet();
            da.Fill(ds);

            CultureInfo culture = new CultureInfo("en");
            foreach (DataRow r in ds.Tables[0].Rows) {
                var lonlat = r["latlong"].ToString().Split(',');
                
                res.Add(new Place {
                    formatted_address = r["fromRoute"].ToString(),
                    lon = double.Parse(lonlat[0], culture),
                    lat = double.Parse(lonlat[1], culture)
                });
            }

            return JsonConvert.SerializeObject(res);
        }
        */
        /*
        [WebMethod]
        public string sendRouteSMS(
            double toLng, 
            double toLat, 
            string phone, 
            string tenant, 
            string estate)
        {
            string sms_address, sms_password, sms_smtp_host;
            int sms_smtp_port = 25;
            bool sms_enable_ssl = false;

            sms_address = WebConfigurationManager.AppSettings["sms_address"].ToString();
            sms_password = WebConfigurationManager.AppSettings["sms_password"].ToString();
            sms_smtp_host = WebConfigurationManager.AppSettings["sms_smtp_host"].ToString();
            Int32.TryParse(WebConfigurationManager.AppSettings["sms_smtp_port"].ToString(), out sms_smtp_port);
            Boolean.TryParse(WebConfigurationManager.AppSettings["sms_enable_ssl"].ToString(), out sms_enable_ssl);

            MailMessage mail = new MailMessage();
            mail.To.Add(phone + "@sms.peie.om");
            mail.From = new MailAddress(phone + "@sms.peie.om", "MAP.PEIE.OM", System.Text.Encoding.UTF8);
            //mail.To.Add(phone);
            //mail.From = new MailAddress(sms_address, "MAP.PEIE.OM", System.Text.Encoding.UTF8);
            mail.Subject = "";
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            if (!String.IsNullOrEmpty(tenant))
            {
                mail.Body += "Destination:" + tenant + "," + estate;
                mail.Body = "Destination: " + tenant + ", " + estate + " , Map Link: " + "http://maps.google.com/maps?q=" + toLat.ToString().Substring(0, 8) + "," + toLng.ToString().Substring(0, 8);
            }
            else
            {
                mail.Body += "Destination:" + estate;
                mail.Body = "Destination: " + estate + " , Map Link: " + "http://maps.google.com/maps?q=" + toLat.ToString().Substring(0, 8) + "," + toLng.ToString().Substring(0, 8);
            }
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = false;

            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential(phone + "@sms.peie.om", sms_password);
            //client.Credentials = new System.Net.NetworkCredential(sms_address, sms_password);
            client.Port = sms_smtp_port;
            client.Host = sms_smtp_host;
            client.EnableSsl = sms_enable_ssl;

            try
            {
                client.Send(mail);
            }
            catch (Exception ex)
            {
                Exception ex2 = ex;
                string errorMessage = string.Empty;
                while (ex2 != null)
                {
                    errorMessage += ex2.ToString();
                    ex2 = ex2.InnerException;
                }

                return Main.Failure_sending_SMS;
            }

            return string.Empty;
        }
        */
        /*
        [WebMethod]
        public string sendMessageSMS(string phone, string message_body)
        {
            string sms_address, sms_password, sms_smtp_host;
            int sms_smtp_port = 25;
            bool sms_enable_ssl = false;

            sms_address = WebConfigurationManager.AppSettings["sms_address"].ToString();
            sms_password = WebConfigurationManager.AppSettings["sms_password"].ToString();
            sms_smtp_host = WebConfigurationManager.AppSettings["sms_smtp_host"].ToString();
            Int32.TryParse(WebConfigurationManager.AppSettings["sms_smtp_port"].ToString(), out sms_smtp_port);
            Boolean.TryParse(WebConfigurationManager.AppSettings["sms_enable_ssl"].ToString(), out sms_enable_ssl);

            MailMessage mail = new MailMessage();
            mail.To.Add(phone + "@sms.peie.om");
            mail.From = new MailAddress(phone + "@sms.peie.om", "MAP.PEIE.OM", System.Text.Encoding.UTF8);
            //mail.To.Add(phone);
            //mail.From = new MailAddress(sms_address, "MAP.PEIE.OM", System.Text.Encoding.UTF8);
            mail.Subject = "";
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = message_body;
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = false;

            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential(phone + "@sms.peie.om", sms_password);
            //client.Credentials = new System.Net.NetworkCredential(sms_address, sms_password);
            client.Port = sms_smtp_port;
            client.Host = sms_smtp_host;
            client.EnableSsl = sms_enable_ssl;

            try
            {
                client.Send(mail);
            }
            catch (Exception ex)
            {
                Exception ex2 = ex;
                string errorMessage = string.Empty;
                while (ex2 != null)
                {
                    errorMessage += ex2.ToString();
                    ex2 = ex2.InnerException;
                }

                return Main.Failure_sending_SMS;
            }

            return string.Empty;
        }
        */
        [WebMethod]
        public string sendMessageSMS(string phone, string message_body)
        {
            try
            {
                WebClient Client = new WebClient();
                String RequestURL, RequestData;

                RequestURL = "https://redoxygen.net/sms.dll?Action=SendSMS";

                RequestData = "AccountId=CI00170872"
                    + "&Email=" + System.Web.HttpUtility.UrlEncode("admin@spasnanovom.ru")
                    + "&Password=" + System.Web.HttpUtility.UrlEncode("1btfIGv3")
                    + "&Recipient=" + System.Web.HttpUtility.UrlEncode(phone)
                    + "&Message=" + System.Web.HttpUtility.UrlEncode(message_body);

                byte[] PostData = Encoding.ASCII.GetBytes(RequestData);
                byte[] Response = Client.UploadData(RequestURL, PostData);

                String Result = Encoding.ASCII.GetString(Response);
                int ResultCode = System.Convert.ToInt32(Result.Substring(0, 4));
            }
            catch (Exception ex)
            {
                //return ResultCode.ToString();
                return Main.Failure_sending_SMS;
            }

            return string.Empty;
        }


        [WebMethod]
        public string sendEmail(string to, string message_subject, string message_body)
        {
            string email_address, email_password, email_smtp_host;
            int email_smtp_port = 25;
            bool email_enable_ssl = false;

            email_address = WebConfigurationManager.AppSettings["email_address"].ToString();
            email_password = WebConfigurationManager.AppSettings["email_password"].ToString();
            email_smtp_host = WebConfigurationManager.AppSettings["email_smtp_host"].ToString();
            Int32.TryParse(WebConfigurationManager.AppSettings["email_smtp_port"].ToString(), out email_smtp_port);
            Boolean.TryParse(WebConfigurationManager.AppSettings["email_enable_ssl"].ToString(), out email_enable_ssl);

            MailMessage mail = new MailMessage();
            mail.To.Add(to);
            mail.From = new MailAddress(email_address, "MAP.PEIE.OM", System.Text.Encoding.UTF8);
            mail.Subject = message_subject;
            mail.SubjectEncoding = System.Text.Encoding.UTF8;
            mail.Body = message_body;
            mail.BodyEncoding = System.Text.Encoding.UTF8;
            mail.IsBodyHtml = true ;
            //mail.Priority = MailPriority.High;

            //https://www.google.com/settings/security/lesssecureapps
            SmtpClient client = new SmtpClient();
            client.Credentials = new System.Net.NetworkCredential(email_address, email_password);
            client.Port = email_smtp_port; // Gmail works on this port
            client.Host = email_smtp_host;
            client.EnableSsl = email_enable_ssl; //Gmail works on Server Secured Layer
            try
            {
                client.Send(mail);
            }
            catch (Exception ex) 
            {
                Exception ex2 = ex;
                string errorMessage = string.Empty; 
                while (ex2 != null)
                {
                    errorMessage += ex2.ToString();
                    ex2 = ex2.InnerException;
                }

                return Main.Failure_sending_email;
            }

            return string.Empty;
        }

        #region new
        [WebMethod]
        public string getTenantById(int Id, string prefix = "en")
        {
            prefix = (prefix == "ar") ? "a" : "e";

            List<Tenant> res = new List<Tenant>();
            string sqlQuery = sqlQuery = "SELECT " +
                        "count(distinct pnu) cnt, tid, Landuse_" + prefix + " Landuse, tname_" + prefix + " Name " +
                        "FROM " + GlobalTable +" " +
                        "WHERE tid = " + Id + " " +
                        "GROUP BY tid, Landuse_" + prefix + ", tname_" + prefix;
            //return JsonConvert.SerializeObject(sqlQuery);

            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, connectionstring);
            DataSet ds = new DataSet();
            da.Fill(ds);

            string temp = string.Empty;
            foreach (DataRow r in ds.Tables[0].Rows)
            {
                res.Add(new Tenant {
                    Id = r.GetValue<int?>("tid"),
                    Name = r.GetValue<string>("Name"),
                    Count = r.GetValue<int>("cnt"),
                    Landuse = r.GetValue<string>("Landuse")
                });

            }
            return JsonConvert.SerializeObject(res);
        }
        #endregion

        [WebMethod]
        public string getFeatureInfo(string url) {
            string res = string.Empty;

            HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(url);
            HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

            using (StreamReader stream = new StreamReader(
                 resp.GetResponseStream(), Encoding.UTF8))
            {
                 res = stream.ReadToEnd();
            }

            
            return res;
        }

        #region old
        [WebMethod]
        public string getCompanyParcels(int tenantId, int estateId) {
            List<string> res = new List<string>();
            
            var sqlQuery = "SELECT distinct [geom].STAsText() as wkt, estateid FROM " + GlobalTable +" WHERE tid = " + tenantId;

            if (estateId > 0)
            {
                sqlQuery += " AND estateid = " + estateId;
            }

            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, connectionstring);
            DataSet ds = new DataSet();
            da.Fill(ds);

            string temp = string.Empty;
            foreach (DataRow r in ds.Tables[0].Rows)
            {
                temp = "POLYGON ((";
                temp += convert(r["wkt"].ToString(), (int)r["estateid"]);
                temp += "))";
                res.Add(temp);
            }

            return JsonConvert.SerializeObject(res);
        }
        [WebMethod]
        public string getSubCategoryParcels(int estateId, int classId, string subCategory, string prefix = "en")
        {
            prefix = (prefix == "ar") ? "a" : "e";

            List<string> res = new List<string>();

            string sqlQuery = string.Empty;
            if (classId == 5)
            {
                sqlQuery = "select distinct geom.STAsText() as wkt from " + GlobalTable +" where replace(replace([prodtype_" + prefix + "],char(13),''),char(10),'')='" + subCategory + "' and estateid=" + estateId;
            }
            else if (classId == 6)
            {
                sqlQuery = "select distinct geom.STAsText() as wkt from " + GlobalTable +" where replace(replace([cnationality_" + prefix + "],char(13),''),char(10),'')='" + subCategory + "' and estateid=" + estateId;
            }

            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, connectionstring);
            DataSet ds = new DataSet();
            da.Fill(ds);

            //foreach (DataRow r in ds.Tables[0].Rows)
            //    res.Add(r["wkt"].ToString());

            string temp = string.Empty;
            foreach (DataRow r in ds.Tables[0].Rows)
            {
                temp = "POLYGON ((";
                temp += convert(r["wkt"].ToString(), estateId);
                temp += "))";
                res.Add(temp);
            }


            return JsonConvert.SerializeObject(res);
        }
        #endregion

        #region helpers
        string convert(string data, int estateId)
        {
            UTMConverter converter;
            UTMWGS84 FConverter;
            converter = new UTMConverter();
            FConverter = new UTMWGS84();
            string temp = string.Empty;
            string[] polygons = data.Split(',');

            if (data.Contains("MULTIPOLYGON"))
            {
                temp = data.Replace("MULTIPOLYGON (((", "");

                temp = temp.Replace(")))", "");
                temp = temp.Replace(")), ((", ";");
                polygons = temp.Split(';');
            }
            else if (data.Contains("POLYGON"))
            {
                temp = data.Replace("POLYGON ((", "");

                temp = temp.Replace("))", "");
                temp = temp.Replace("), (", ";");
                polygons = temp.Split(';');

            }
            temp = data.Substring(10, data.Length - 13);

            StringBuilder result = new StringBuilder();
            StringBuilder sb = new StringBuilder();

            string[] UTMXY;
            string[] xy;
            double[] latlon;
            int i;

            CultureInfo culture = new CultureInfo("en");
            for (int a = 0; a < polygons.Length; a++)
            {
                UTMXY = polygons[a].Split(',');
                for (i = 0; i < UTMXY.Length; i++)
                {
                    UTMXY[i] = UTMXY[i].Trim();
                    xy = UTMXY[i].Split(' ');

                    if (estateId == 4 || estateId == 9)
                    {
                        latlon = FConverter.WGS84_39N(double.Parse(xy[0], culture), double.Parse(xy[1], culture));
                    }
                    else if (estateId == 3 || estateId == 8 || estateId == 10)
                    {
                        latlon = converter.UTMtoGeographic(double.Parse(xy[0], culture), double.Parse(xy[1], culture));
                    }
                    else
                    {
                        latlon = converter.UTMtoLatLon(double.Parse(xy[0], culture), double.Parse(xy[1], culture));
                    }
                    sb.Append(String.Format("{0} {1},", latlon[1].ToString().Replace(',', '.'), latlon[0].ToString().Replace(',', '.')));
                }
                if (sb.Length > 0)
                {
                    sb.Replace(",", "", sb.Length - 1, 1);
                    //result.Append(sb.ToString());
                    result.Append(sb.ToString() + ",");
                    sb.Clear();
                    //sb = new StringBuilder();
                }
            }
            if (result.Length > 0)
            {
                result.Replace(",", "", result.Length - 1, 1);
            }
            return result.ToString();
        }
        #endregion

        [WebMethod]
        public string getSettings(string prefix = "en")
        {
            Settings res = new Settings();

            var sqlQuery = "SELECT " + prefix + "_welcomeText welcomeText,"
                + prefix + "_tabTitle_1 tabTitle_1,"
                + prefix + "_tabContent_1 tabContent_1,"
                + prefix + "_tabTitle_2 tabTitle_2,"
                + prefix + "_tabContent_2 tabContent_2,"
                + prefix + "_tabTitle_4 tabTitle_4,"
                + prefix + "_tabContent_4 tabContent_4,"
                + prefix + "_help help"
                + " FROM " + SettingsTable +"";
            SqlDataAdapter da = new SqlDataAdapter(sqlQuery, connectionstring);
            DataSet ds = new DataSet();
            da.Fill(ds);

            string temp = string.Empty;
            if(ds.Tables[0].Rows.Count > 0) {
                DataRow r = ds.Tables[0].Rows[0];

                res.welcomeText = r["welcomeText"].ToString();
                res.tabTitle_1 = r["tabTitle_1"].ToString();
                res.tabContent_1 = r["tabContent_1"].ToString();
                res.tabTitle_2 = r["tabTitle_2"].ToString();
                res.tabContent_2 = r["tabContent_2"].ToString();
                res.tabTitle_4 = r["tabTitle_4"].ToString();
                res.tabContent_4 = r["tabContent_4"].ToString();
                res.help = r["help"].ToString();
            }

            return JsonConvert.SerializeObject(res);
        }

    }
}
