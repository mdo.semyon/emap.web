﻿window.EMAP = window.EMAP || {};

EMAP.Enums = {
    Default: 0,
    LandUse: 1,
    Vacancy: 2,
    TenantStage: 3,
    OwnershipType: 4,
    ProductType: 5,
    Investment: 6
};
    

(function (EMAP_Constants, $, undefined) {

    $.extend(true, EMAP_Constants, {
        from_arr: [
            { id: 0, formatted_address_ar: 'الموقع الحالي', formatted_address_en: 'Current Location' },
            { id: 1, formatted_address_ar: 'مطرح', formatted_address_en: 'MUTRAH', lon: 58.5656561600, lat: 23.6191786557 },
            { id: 2, formatted_address_ar: 'بوشر', formatted_address_en: 'BAWSHAR', lon: 58.4052516598, lat: 23.5830615123 },
            { id: 3, formatted_address_ar: 'السيب', formatted_address_en: 'AS SEEB', lon: 58.1798197294, lat: 23.6841198776 },
            { id: 4, formatted_address_ar: 'العامرات', formatted_address_en: 'AL AMRAT', lon: 58.4971714787, lat: 23.4800639223 },
            { id: 5, formatted_address_ar: 'مسقط', formatted_address_en: 'MUSCAT', lon: 58.5948230224, lat: 23.6165736916 },
            { id: 6, formatted_address_ar: 'قريات', formatted_address_en: 'QURAYYAT', lon: 58.9158965658, lat: 23.2615020237 },
            { id: 7, formatted_address_ar: 'صحار', formatted_address_en: 'SOHAR', lon: 56.7497755249, lat: 24.3605771925 },
            { id: 8, formatted_address_ar: 'الرستاق', formatted_address_en: 'AR RUSTAQ', lon: 57.4302679633, lat: 23.3974475068 },
            { id: 9, formatted_address_ar: 'شناص', formatted_address_en: 'SHINAS', lon: 56.4716678732, lat: 24.7427970519 },
            { id: 10, formatted_address_ar: 'لوى', formatted_address_en: 'LIWA', lon: 56.5657392144, lat: 24.5227598180 },
            { id: 11, formatted_address_ar: 'الخابورة', formatted_address_en: 'AL KHABURAH', lon: 57.1045534918, lat: 23.9821506235 },
            { id: 12, formatted_address_ar: 'السويق', formatted_address_en: 'AS SUWAYQ', lon: 57.4384365454, lat: 23.8496506015 },
            { id: 13, formatted_address_ar: 'نخل', formatted_address_en: 'NAKHAL', lon: 57.8354484081, lat: 23.3955314396 },
            { id: 14, formatted_address_ar: 'العوابي', formatted_address_en: 'AL AWABI', lon: 57.5317622172, lat: 23.3106682209 },
            { id: 15, formatted_address_ar: 'المصنعة', formatted_address_en: 'AL MUSANAAH', lon: 57.6386820041, lat: 23.7814024356 },
            { id: 16, formatted_address_ar: 'بركاء', formatted_address_en: 'BARKA', lon: 57.8917240918, lat: 23.7068831405 },
            { id: 17, formatted_address_ar: 'صحم', formatted_address_en: 'SAHAM', lon: 56.8850195642, lat: 24.1768324658 },
            { id: 18, formatted_address_ar: 'خصب', formatted_address_en: 'KHASAB', lon: 56.2458190003, lat: 26.1921822360 },
            { id: 19, formatted_address_ar: 'بخا', formatted_address_en: 'BUKHA', lon: 56.1543721742, lat: 26.1426573798 },
            { id: 20, formatted_address_ar: 'دبا البيعة', formatted_address_en: 'DABA AL BAYAH', lon: 56.2682393129, lat: 25.6353826988 },
            { id: 21, formatted_address_ar: 'مدحا', formatted_address_en: 'MADHA', lon: 56.3280317474, lat: 25.2803566158 },
            { id: 22, formatted_address_ar: 'البريمي', formatted_address_en: 'AL BURAYMI', lon: 55.7856259035, lat: 24.2502438611 },
            { id: 23, formatted_address_ar: 'عبري', formatted_address_en: 'IBRI', lon: 56.5164530865, lat: 23.2168217459 },
            { id: 24, formatted_address_ar: 'مدحاء', formatted_address_en: 'MAHDAH', lon: 55.9675472193, lat: 24.4027363437 },
            { id: 25, formatted_address_ar: 'ينقل', formatted_address_en: 'YANQUL', lon: 56.5450408818, lat: 23.5896261698 },
            { id: 26, formatted_address_ar: 'ضنك', formatted_address_en: 'DANK', lon: 56.2728766545, lat: 23.5642459829 },
            { id: 27, formatted_address_ar: 'نزوى', formatted_address_en: 'NIZWA', lon: 57.5286431289, lat: 22.9367298099 },
            { id: 28, formatted_address_ar: 'سمائل', formatted_address_en: 'SAMAIL', lon: 58.0029640160, lat: 23.3073867716 },
            { id: 29, formatted_address_ar: 'بهلا', formatted_address_en: 'BAHLA', lon: 57.2954203219, lat: 22.9667744354 },
            { id: 30, formatted_address_ar: 'أدم', formatted_address_en: 'ADAM', lon: 57.5174460798, lat: 22.3940525531 },
            { id: 31, formatted_address_ar: 'الحمراء', formatted_address_en: 'AL HAMRA', lon: 57.2948044968, lat: 23.1210817259 },
            { id: 32, formatted_address_ar: 'منح', formatted_address_en: 'MANAH', lon: 57.6048011771, lat: 22.8057049420 },
            { id: 33, formatted_address_ar: 'إزكي', formatted_address_en: 'IZKI', lon: 57.7612456602, lat: 22.9318627432 },
            { id: 34, formatted_address_ar: 'بدبد', formatted_address_en: 'BID BID', lon: 58.1262170886, lat: 23.4098761815 },
            { id: 35, formatted_address_ar: 'صور', formatted_address_en: 'SUR', lon: 59.5303200048, lat: 22.5664566514 },
            { id: 36, formatted_address_ar: 'إبراء', formatted_address_en: 'IBRA', lon: 58.5304395833, lat: 22.7031245557 },
            { id: 37, formatted_address_ar: 'القابل', formatted_address_en: 'AL QABIL', lon: 58.6974657845, lat: 22.5681878973 },
            { id: 38, formatted_address_ar: 'المضيبي', formatted_address_en: 'AL MUDAYBI', lon: 58.1293242672, lat: 22.5774898060 },
            { id: 39, formatted_address_ar: 'محلان', formatted_address_en: 'MAHLAH', lon: 58.6047356566, lat: 23.0461989480 },
            { id: 40, formatted_address_ar: 'الكامل والوافي', formatted_address_en: 'AL KAMIL WA AL WAFI', lon: 59.2175570373, lat: 22.1986699609 },
            { id: 41, formatted_address_ar: 'جعلان بني بو علي', formatted_address_en: 'JAALAN BANI BU ALI', lon: 59.3445960050, lat: 22.0313343162 },
            { id: 42, formatted_address_ar: 'جعلان بني بو حسن', formatted_address_en: 'JAALAN BANI BU HASAN', lon: 59.2975564617, lat: 22.0724748196 },
            { id: 43, formatted_address_ar: 'هيما', formatted_address_en: 'HAYMA', lon: 56.2931473016, lat: 19.9579934274 },
            { id: 44, formatted_address_ar: 'صلالة', formatted_address_en: 'SALALAH', lon: 54.0934972722, lat: 17.0148966266 },
            { id: 45, formatted_address_ar: 'طاقة', formatted_address_en: 'TAQAH', lon: 54.3902551823, lat: 17.0433216485 },
            { id: 46, formatted_address_ar: 'سدح', formatted_address_en: 'SADH', lon: 55.0635944339, lat: 17.0503797722 },
            { id: 47, formatted_address_ar: 'رخيوت', formatted_address_en: 'RAKHYUT', lon: 53.4120517315, lat: 16.7479285091 },
            { id: 48, formatted_address_ar: 'ضلكوت', formatted_address_en: 'DALKUT', lon: 53.2501764535, lat: 16.7102666451 },
            { id: 49, formatted_address_ar: 'مقشن', formatted_address_en: 'MUQSHIN', lon: 54.9159845322, lat: 19.5624116861 },
            { id: 50, formatted_address_ar: 'شليم وجزر الحلانيات', formatted_address_en: 'SHALIM', lon: 55.6485358684, lat: 18.2261281279 },
            { id: 51, formatted_address_ar: 'مرباط', formatted_address_en: 'MIRBAT', lon: 54.6968824572, lat: 16.9883482046 },
            { id: 52, formatted_address_ar: 'ثمريت', formatted_address_en: 'THUMRAYT', lon: 54.0413120030, lat: 17.6230636317 }
        ],
        layersBounds: [{
            id: 2,
            bounds: [6475236.78429, 2697127.64345, 6482983.79421, 2700802.26430]
        },
        {
            id: 6,
            bounds: [6602255.678396744, 2581635.992502869, 6624269.542542874, 2592929.5634320043]
        },
        {
            id: 4,
            bounds: [5992775.362547418, 1914144.4411867196, 6018878.607706806, 1921253.084817241]
        },
        {
            id: 5,
            bounds: [6396295.513824107, 2611600.103807248, 6401798.979860639, 2614423.496539532]
        },
        {
            id: 9,
            bounds: [5853532.771218555, 2015252.2056985663, 5859036.237255087, 2018075.5984308498]
        },
        {
            id: 7,
            bounds: [6208086.339350817, 2786313.0565143838, 6219093.271423883, 2791959.841978951]
        },
        {
            id: 3,
            bounds: [6281570.614966515, 2798180.754675665, 6303584.479112646, 2809474.3256048]
        },
        {
            id: 10,
            bounds: [6468033.03890074, 2662718.248327237, 6479039.970973806, 2668365.033791804]
        },
        {
            id: 8,
            bounds: [6473545.418199125, 2699930.5395656545, 6476808.323844048, 2701466.44610276]//[6471464.897835484, 2699547.1600956614, 6477990.709125331, 2701324.321003292]
        }],
        applicationInfo: "The Estate Map is intended to help you in quickly locating the industrial units located within PEIE premises. <br> You can use the search option to find the location of a particular company. <br> The route tool can be used to identify the route from a location of interest to a particular estate or a company. Having reached the industrial estate you could zoom and see the overall layout and locate the factory of your interest. <br> By using the `Classifications’ menu, you can take a snapshot of vacant lands or sector focus or any such classification provided in the drop down menu. VThe Estate Map also gives you some static and dynamic data about each industrial estate.",
        estateStaticData: [{
            id: 2,
            name: 'Rusayl',
            area: '7494841',
            rent: '500 Baizas per m2 annually',
            highway: 'Rusayl – Nizwa',
            airport: 'Muscat International Airport - 10 kms',
            port: 'Mina Sultan Qaboos - 45kms',
            electricity: 'Muscat  Electricity Company',
            gas: 'Oman Gas Company',
            water: 'Public authority for electricity and water',
            focus: '',
            address: 'Mohseen Al Hinai Director General Rusayl Industrial Estate PO Box 2, 124, Rusayl Tel: (+968) 24 44 96 00 Fax: (+968) 24 44 96 01 E-mail: rusayl@peie.om',
        },
        {
            id: 3,
            name: 'Sohar',
            area: '21674348',
            rent: '500 Baizas per m2 annually',
            highway: 'Batinah North',
            airport: 'Muscat International Airport - 200 kms',
            port: 'Sohar Port - 45kms',
            electricity: 'Majan Electricity Company',
            gas: 'Oman Gas Company',
            water: 'Bore wells',
            focus: '',
            address: 'Eng. Hamad bin Salim Al Mahdali Director General Sohar Industrial Estate PO Box 1, 327, Sohar Tel: (+968) 26 75 1272 Fax: (+968) 26 75 1307 E-mail: sohar@peie.com'
        },
        {
            id: 4,
            name: 'Raysut',
            area: '2138198',
            rent: '250 Baizas per m2 annually',
            highway: 'Salalah - Raysut',
            airport: 'Salalah  Airport- 15 kms',
            port: 'Port Salalah- 4kms',
            electricity: 'Dhofar Power Company',
            gas: 'Ministry of Oil and Gas',
            water: 'Office of the Minister of State and Governer of Dhofar',
            focus: '',
            address: 'Eng. Said bin Ali Al-Mashani Director General Raysut Industrial Estate PO Box 2317, 211, Salalah Tel: (+968) 23 218311 Fax: (+968) 23 21 9221 E-mail: raysut@peie.com'
        },
        {
            id: 6,
            name: 'Sur',
            area: '36100000',
            rent: '250 Baizas per m2 annually',
            highway: 'Sur - Muscat',
            airport: 'Muscat International Airport - 200kms',
            port: 'Sultan Qaboos Port- 160kms',
            electricity: 'Mazoon Electricity Company',
            gas: 'Petroleum Development Oman PDO',
            water: 'Public authority for electricity and water',
            focus: '',
            address: 'Eng. Abdullah Khamis Al-Mukhaini Director General Sur Industrial Estate PO Box 959, 411, Sur Tel: (+968) 25 56 22 92 – 25 56 22 93 Fax: (+968) 25 56 22 90 E-mail: sur@peie.om'
        },
        {
            id: 5,
            name: 'Nizwa',
            area: '3079935',
            rent: '250 Baizas per m2 annually',
            highway: 'Nizwa – Salalah',
            airport: 'Muscat International Airport- 140 kms',
            port: 'Mina Sultan Qaboos -200kms',
            electricity: 'Mazoon Electricity Company',
            gas: 'Oman Gas Company',
            water: 'Well water from Izz',
            focus: '',
            address: 'Eng. Hamad AL-Qasabi  Director General Nizwa Industrial Estate PO Box 998, 611, Nizwa Tel: (+968)25212200 Fax: (+968) 25 44 9119 E-mail: nizwa@peie. om'
        },
        {
            id: 7,
            name: 'Buraimi',
            area: '5500000',
            rent: '250 Baizas per m2 annually',
            highway: 'Al Buraimi - Mahdah',
            airport: 'Dubai International Airport- 120 km',
            port: 'Sohar Port- 120km',
            electricity: 'Majan Electricity Company',
            gas: 'N/A',
            water: 'Public Authority of Electricity & Water',
            focus: '',
            address: 'Eng. Abdullah bin Salim AlKabi General Director of Al Buraimi Industrial Estate PO Box 525, 512, Al Buraimi Tel: (+968) 25 65 77 77 Fax: (+968) 25 64 20 33 E-mail: buraimi@peie.om'
        },
        {
            id: 9,
            name: 'Mazunah',
            area: '4500000',
            rent: '1.0 O.R. per M2 per year for Land.',
            highway: 'Al Mazunah - Thamriat',
            airport: 'Salalah Airport - 250 kms',
            port: 'Salalah Port - 270 kms',
            electricity: 'Rural Areas Electicity Company',
            gas: '',
            water: 'Directorate General of Water (Governorate of Dhofar)',
            focus: '',
            address: 'Mazunah Free Zone PO Box 2317, 211, Raysut Tel: (+968) 23 27 1441, Fax: (+968) 23 27 1443'
        },
        {
            id: 8,
            name: 'KOM',
            area: '814039',
            rent: 'RO 6 per m2 per month inclusive of maintenance.RO 0.500 per m2 1st year RO1 per m2 /year for subsequent years.TKMs Monthly Office Rates 1st 6 months are free.RO 3 per m2 inclusive of power and maintenance.',
            highway: 'Muscat - Nizwa',
            airport: 'Muscat Airport- 12 kms',
            port: 'Mina Sultan Qaboos Port- 60 kms',
            electricity: 'Muscat  Electricity Company',
            gas: 'N/A',
            water: 'KOM',
            focus: 'ICT',
            address: 'Call Center: 24170070'
        },
        {
            id: 10,
            name: 'Sumail',
            area: '7315775',
            rent: 'First year: 250 Baizas per m2, Second year: 500 Baizas per m2',
            highway: 'Muscat-Nizwa Highway, Ashurqaya Highway',
            airport: 'Muscat International Airport - 45 kms',
            port: 'Mina Sultan Qaboos - 80kms',
            electricity: 'Mazoon Electricity Company SAOC',
            gas: 'No Gas',
            water: 'Public authority for electricity and water',
            focus: 'Food, Building materials, Steel, Plastic, Marbles, High Technology',
            address: 'Al Dakhiliyah governorate, Sumail, Phone: (+968) 25362000, email: dawood@peie.om'
        }]
    });

}(window.EMAP.Constants = window.EMAP.Constants || {}, jQuery));
