angular.module('uiRouterGIS.filters', [])
.filter('normalizePhone', function() {
	return function(input) {
		return input + '_norm';
	};
})
.filter('trimPhone', function() {
	return function(input) {
		return input + '_trim';
	};
})
.filter('count', function() {
	return function(input) {
		return input ? input.length : '';
	};
})
.filter('trimmer', [function () {
	return function (input, params) {
		var filtered = "";

		if(input && params[0] == "_collapse")
			filtered = input.substr(0, params[1]) + '...';
		else
			filtered = input;

		return filtered;
	}
}])
.filter('nth_item', [function () {
	return function (items, params) {
		var filtered = "";

		if(items && items.length >= params[0]) {
			var item = items[params[0]];
			
			if (item[params[1]]) {
				
				filtered = item[params[1]];
				
				if(params.length >= 3 && params[2] > 0)
					filtered = filtered.substr(0, params[2]);
			}
		}

		return filtered;
	}
}]);

