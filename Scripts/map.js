﻿(function (EMAP, $, undefined) {
    $.extend(true, EMAP, {
        Layers: [],
        createTextStyle: function(text) {
            var align = 'center';
            var baseline = 'Alphabetic';
            var size = '14px';
            var offsetX = 0;
            var offsetY = -10;
            var weight = 500;
            var rotation = 0;
            var font = weight + ' ' + size + ' ' + 'Arial';
            var fillColor = '#fff';
            var outlineColor = '#000';
            var outlineWidth = 1;

            return new ol.style.Text({
                textAlign: align,
                textBaseline: baseline,
                font: font,
                text: text,
                fill: new ol.style.Fill({color: fillColor}),
                stroke: new ol.style.Stroke({color: outlineColor, width: outlineWidth}),
                offsetX: offsetX,
                offsetY: offsetY,
                rotation: rotation
            });
        },
        makeLayers: function () {
            this.makeRasterLayers();
            this.makeIconLayer();

            this.Wms.addLayer('l2', 2);
            this.Wms.addLayer('l3', 3);
            this.Wms.addLayer('l4', 4);
            this.Wms.addLayer('l5', 5);
            this.Wms.addLayer('l6', 6);
            this.Wms.addLayer('l7', 7);
            this.Wms.addLayer('l8', 8);
            this.Wms.addLayer('l9', 9);
            this.Wms.addLayer('l10', 10);
        },
        makeIconLayer: function () {
            this.iconsSource = new ol.source.Vector();
            this.iconsLayer = new ol.layer.Vector({
                isIcons: true,
                source: this.iconsSource
            });

            this.Layers.push(this.iconsLayer);
        },
        makeRasterLayers: function () {

            if (EMAP.Settings.map_type && EMAP.Settings.map_type == "google") {
                
                /*
                    h = roads only
                    m = standard roadmap
                    p = terrain
                    r = somehow altered roadmap
                    s = satellite only
                    t = terrain only
                    y = hybrid
                */

                var mapTypeId = "";
                switch (EMAP.Settings.map_mode) {
                    case "ROADMAP":
                        mapTypeId = "m";
                        break;
                    case "SATELLITE":
                        mapTypeId = "s";
                        break;
                    case "HYBRID":
                        mapTypeId = "y";
                        break;
                    case "TERRAIN":
                        mapTypeId = "t";
                        break;
                    default:
                        mapTypeId = "m";
                }

                /*
                this.Layers.push(new olgm.layer.Google({
                    visible: false,
                    isRaster: true,
                    mapTypeId: mapTypeId
                }));

                */
                    
                this.Layers.push(new ol.layer.Tile({
                    visible: false,
                    isRaster: true,
                    preload: Infinity,
                    source: new ol.source.XYZ({
                        //attributions: [attribution],
                        url: 'http://mt1.google.com/vt/lyrs=' + mapTypeId + '&hl=' + EMAP.Localization.Culture + '&x={x}&y={y}&z={z}'
                    })
                }));

            }
            else {
                this.Layers.push(new ol.layer.Tile({
                    visible: false,
                    isRaster: true,
                    preload: Infinity,
                    source: new ol.source.BingMaps({
                        key: EMAP.Settings.bingKey,
                        imagerySet: EMAP.Settings.map_mode //'AerialWithLabels'//'Road'//'Aerial'//'Road'
                    })
                }));
            }
        },
        createInfoTooltip: function () {
            var me = this;

            this.popupContainer = document.createElement('div');
            this.popupContainer.className = 'ol-popup';
            this.popupContainer.setAttribute("id", "popup");

            this.popupCloser = document.createElement('a');
            this.popupCloser.className = 'ol-popup-closer';
            this.popupCloser.setAttribute("id", "popup-closer");

            this.popupContent = document.createElement('div');
            this.popupContent.setAttribute("id", "popup-content");

            this.popupContainer.appendChild(this.popupCloser);
            this.popupContainer.appendChild(this.popupContent);

            this.infoTooltip = new ol.Overlay({
                element: this.popupContainer,
                autoPan: true,
                autoPanAnimation: {
                    duration: 250
                }
            });

            this.popupCloser.onclick = function () {
                me.infoTooltip.setPosition(undefined);
                me.popupCloser.blur();
                EMAP.Vector.removeFeatures();
                return false;
            };

            EMAP.map.addOverlay(this.infoTooltip);
        },
        initStartMap: function () {

            var me = this;

            this.makeRasterLayers();
            this.map = new ol.Map({
                layers: this.Layers,
                renderer: common.getRendererFromQueryString(),
                loadTilesWhileInteracting: true,
                target: 'map',
                logo: false,
                controls: [],
                view: new ol.View({
                    center: [6132795.443472614, 2442274.4548305715],
                    zoom: 6
                })
            });

            this.Layers[0].setVisible(true);


        },
        initMap: function () {
            var me = this;

            this.makeLayers();
            this.map = new ol.Map({
                layers: this.Layers,
                renderer: common.getRendererFromQueryString(),
                loadTilesWhileInteracting: true,
                target: 'map',
                logo: false,
                //controls: [],
                view: new ol.View({
                    center: [6132795.443472614, 2442274.4548305715],
                    zoom: 6
                })
            });

            this.createInfoTooltip();
            this.Layers[0].setVisible(true);
        },
        showTenantDetailsProxy: function (Id) {
            var scope = angular.element('body').scope();
            if (scope) {
                scope.contentScreen.showTenantDetails(Id);
            }
        },
        routeToPointProxy: function (lon, lat, tenantId) {
            var scope = angular.element('body').scope();
            if (scope) {
                scope.contentScreen.routeToPoint(lon, lat, tenantId);
            }
        }
    });

}(window.EMAP = window.EMAP || {}, jQuery));

(function (EMAP_Wms, $, undefined) {

    $.extend(true, EMAP_Wms, {
        addLayer: function (estateName, layerId) {

            var res = jQuery.grep(EMAP.Constants.layersBounds, function (item) {
                if (item.id == layerId)
                    return true;
            });

            var wms = new ol.layer.Image({
                estateID: layerId,
                title: estateName,
                opacity: 0.5,
                extent: res[0].bounds,
                visible: false,
                maxResolution: 152.8740565703525,
                //minResolution:
                source: new ol.source.ImageWMS({
                    url: EMAP.Settings.wmsURL,
                    params: {
                        'LAYERS': EMAP.Settings.workspace + ':vacancy' + layerId,
                        styles: '',
                        srs: 'EPSG:900913'
                    },
                    serverType: 'geoserver'
                })
            });

            EMAP.Layers.push(wms);
        },
        showlayer: function (layerId, criteria) {
            if (!EMAP.Layers || EMAP.Layers.length == 0)
                return;

            if (criteria) {
                for (var i = 0; i < EMAP.Layers.length; i++) {
                    var layer = EMAP.Layers[i];

                    if (layer.get('isRaster') || layer.get('isIcons'))
                        continue;

                    if (layer.get('hasCriteria') && layer.get('estateID') == layerId && layer.get('title') == criteria)
                        this.fitToLayerExtent(layer);
                    else
                        layer.setVisible(false);
                }
            }
            else {
                for (var i = 0; i < EMAP.Layers.length; i++) {
                    var layer = EMAP.Layers[i];

                    if (layer.get('isRaster') || layer.get('isIcons'))
                        continue;

                    if (layer.get('hasCriteria')) {
                        layer.setVisible(false);
                        continue;
                    }

                    if (layer.get('estateID') == layerId)
                        this.fitToLayerExtent(layer);
                    else
                        layer.setVisible(false);
                }
            }
        },
        fitToLayerExtent: function (layer) {
            
            var view = EMAP.map.getView(),
                ext = layer.getExtent();

            if (ext) {
                
                view.fit(ext, EMAP.map.getSize());
                layer.setVisible(true);

                /*
                var eID = layer.get('estateID');
                console.log(eID);
                switch (eID) {
                    case 9:
                    case 5:
                        view.setZoom(15);
                        break;
                    case 3:
                        view.setZoom(13);
                        break;
                    default:
                        view.setZoom(14);
                        break;
                }
                */

                this.activeWmsLayer = layer;
            }
            else {
                view.setCenter(ol.proj.transform([52.61922300931735, 17.83631572735013], 'EPSG:4326', view.getProjection()));
                view.setZoom(13);
            }
            
        },
        fitToLayerExtentByID: function (layerId, classId) {

            for (var i = 0; i < EMAP.Layers.length; i++) {
                var layer = EMAP.Layers[i];

                if (layer.get('isRaster') || layer.get('isIcons'))
                    continue;

                if (!layer.get('visible'))
                    continue;

                if (layer.get('estateID') == layerId)
                    this.fitToLayerExtent(layer);
            }

        },
        addClassificationLayer: function (classId, layerId) {

            this.clearClassification();

            var layer = jQuery.grep(EMAP.Layers, function (item) {
                if (item.get('hasCriteria') && item.get('estateID') == layerId && item.get('title') == classId)
                    return true;
            });

            if (layer && layer.length > 0) {
                this.showlayer(layerId, classId);
                return;
            }

            var layername = undefined;
            switch (classId) {
                case EMAP.Enums.LandUse:
                    layername = EMAP.Settings.workspace + ':landuse' + layerId;
                    break;
                case EMAP.Enums.TenantStage:
                    layername = EMAP.Settings.workspace + ':appstage' + layerId;
                    break;
                //case EMAP.Enums.Vacancy:
                //    layername = EMAP.Settings.workspace + ':vacancy' + layerId;
                //    break;
                case EMAP.Enums.OwnershipType:
                    layername = EMAP.Settings.workspace + ':captype' + layerId;
                    break;
                default: break;
            }

            
            var res = jQuery.grep(EMAP.Constants.layersBounds, function (item) {
                if (item.id == layerId)
                    return true;
            });
            
            var wms = new ol.layer.Image({
                estateID: layerId,
                hasCriteria: true,
                title: classId,
                opacity: 0.5,
                visible: true,
                extent: res[0].bounds,
                maxResolution: 152.8740565703525,
                //minResolution: 2.39,
                source: new ol.source.ImageWMS({
                    url: EMAP.Settings.wmsURL,
                    params: {
                        'LAYERS': layername,
                        styles: '',
                        srs: 'EPSG:900913'
                    },
                    serverType: 'geoserver'
                })
            });

            EMAP.Layers.push(wms);
            EMAP.map.addLayer(wms);

            this.showlayer(layerId, classId);
        },
        clearClassification: function () {
            for (var i = 0; i < EMAP.Layers.length; i++) {
                var layer = EMAP.Layers[i];

                if (layer.get('hasCriteria'))
                    layer.setVisible(false);
            }
        },
        hideIcons: function () {
            for (var i = 0; i < EMAP.Layers.length; i++) {
                var layer = EMAP.Layers[i];

                if (layer.get('isIcons'))
                    layer.setVisible(false);
            }
        },
        showIcons: function () {
            for (var i = 0; i < EMAP.Layers.length; i++) {
                var layer = EMAP.Layers[i];

                if (layer.get('isIcons'))
                    layer.setVisible(true);
            }
        }
    });

}(window.EMAP.Wms = window.EMAP.Wms || {}, jQuery));

(function (EMAP_Route, $, undefined) {

    $.extend(true, EMAP_Route, {
        init: function() {
            this.directionService = new google.maps.DirectionsService();

            this.routeSource = new ol.source.Vector();
            this.routeLayer = new ol.layer.Vector({
                source: this.routeSource,
                visible: true,
                style: new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: 'transparent'
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'rgba(255, 0, 0, 1)', //'red',
                        width: 1
                    })
                })
            });

            EMAP.map.addLayer(this.routeLayer);
        },
        findRoute: function (src, dst) {
            var me = this;

            $('.loading-spiner-holder').css('display', 'block');

            var origin = new google.maps.LatLng(src.lat, src.lon),
                destination = new google.maps.LatLng(dst.lat, dst.lon);

            this.directionService.route({
                origin: origin,
                destination: destination,
                avoidHighways: false,
                avoidTolls: false,
                provideRouteAlternatives : false,
                unitSystem: google.maps.UnitSystem.METRIC, //google.maps.UnitSystem.IMPERIAL
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            },
            function (directionsResult, directionsStatus) {
                $('.loading-spiner-holder').css('display', 'none');

                if (directionsStatus == google.maps.DirectionsStatus.OK) {

                    me.results = directionsResult;
                    me.displayDirections(directionsResult);
                    me.drawDirections(directionsResult);

                } else if (directionsStatus == google.maps.DirectionsStatus.OVER_QUERY_LIMIT) {
                    console.log('OVER_QUERY_LIMIT');
                } else {
                    alert("Request failed");
                }
            });
        },
        t_findRoute: function (src, dst) {
            var me = this;

            $('.loading-spiner-holder').css('display', 'block');

            var origin = new google.maps.LatLng(src.lat, src.lon),
                destination = new google.maps.LatLng(dst.lat, dst.lon);

            this.directionService.route({
                origin: origin,
                destination: destination,
                avoidHighways: false,
                avoidTolls: false,
                provideRouteAlternatives: false,
                unitSystem: google.maps.UnitSystem.METRIC, //google.maps.UnitSystem.IMPERIAL
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            },
            function (directionsResult, directionsStatus) {
                $('.loading-spiner-holder').css('display', 'none');

                if (directionsStatus == google.maps.DirectionsStatus.OK) {

                    me.results = directionsResult;
                    me.t_displayDirections(directionsResult);
                    me.drawDirections(directionsResult);

                } else if (directionsStatus == google.maps.DirectionsStatus.OVER_QUERY_LIMIT) {
                    console.log('OVER_QUERY_LIMIT');
                } else {
                    alert("Request failed");
                }
            });
        },
        displayDirections: function(result){
            //clear old data
            $("#emap-route-directions").html('');

            //var html = '<div class="table-responsive">';
            var html = '<table class="table">';
            html += '<tr><td colspan="3">' + $.trim(result.routes[0].legs[0].distance.text.replace(/"/g, '')) + ', ' + $.trim(result.routes[0].legs[0].duration.text.replace(/"/g, '')) + '</td></tr>';

         
            //$("#emap-route-directions").html(html);
            //Display Steps
            var steps  = result.routes[0].legs[0].steps;
            for(i = 0; i < steps.length; i++) {
                var instructions = JSON.stringify(steps[i].instructions);
                var distance = JSON.stringify(steps[i].distance.text);
                var time = JSON.stringify(steps[i].duration.text);
                //$("#emap-route-directions").append(this.getEmbedHTML(i + 1, instructions, distance, time));
                html += this.getEmbedHTML(i + 1, instructions, distance, time);
            }
            html += '</table>';
            //html += '</div>';

            $("#emap-route-directions").html(html);
        },
        t_displayDirections: function (result) {
            //clear old data
            $("#emap-t_route-directions-" + EMAP.Localization.Culture).html('');

            //var html = '<div class="table-responsive">';
            var html = '<table class="table">';
            html += '<tr><td colspan="3">' + $.trim(result.routes[0].legs[0].distance.text.replace(/"/g, '')) + ', ' + $.trim(result.routes[0].legs[0].duration.text.replace(/"/g, '')) + '</td></tr>';


            //$("#emap-route-directions").html(html);
            //Display Steps
            var steps = result.routes[0].legs[0].steps;
            for (i = 0; i < steps.length; i++) {
                var instructions = JSON.stringify(steps[i].instructions);
                var distance = JSON.stringify(steps[i].distance.text);
                var time = JSON.stringify(steps[i].duration.text);
                //$("#emap-route-directions").append(this.getEmbedHTML(i + 1, instructions, distance, time));
                html += this.getEmbedHTML(i + 1, instructions, distance, time);
            }
            html += '</table>';
            //html += '</div>';

            $("#emap-t_route-directions-" + EMAP.Localization.Culture).html(html);
        },
        getEmbedHTML: function(seqno,instructions,distance,duration) {
            var strhtml = '<tr onclick="EMAP.Route.gotoStep('+ (seqno - 1) +')" style="cursor:pointer">';
            strhtml += '<th>' + seqno + '</th>';
            strhtml += '<td>' + $.trim(instructions.replace(/"/g, '')) + '</td>';
            strhtml += '<td>' + $.trim(distance.replace(/"/g, '')) + ' </td>';
            strhtml += '</tr>';
         
            return strhtml;
        },
        drawDirections: function (result) {
            this.routeSource.clear();
            var path = result.routes[0].overview_path;

            var coordinates = [];
            for (var i = 0; i < path.length; i++) {
                var lng = path[i].lng(),
                    lat = path[i].lat();
                coordinates.push([lng, lat]);
            }

            
            var geom = new ol.geom.LineString(coordinates);
            var feature = new ol.Feature({
                geometry: geom.transform('EPSG:4326', 'EPSG:3857'),
                name: 'route path'
            });

            this.routeSource.addFeature(feature);

            var extent = this.routeSource.getExtent();
            this.fitExtent(extent);
        },
        fitExtent: function (extent) {
            var view = EMAP.map.getView();

            view.fit(extent, EMAP.map.getSize());
        },
        gotoStep: function (i) {
            var step = this.results.routes[0].legs[0].steps[i];
            var center = [(step.start_location.lng() + step.end_location.lng()) / 2, (step.start_location.lat() + step.end_location.lat()) / 2];

            var view = EMAP.map.getView();

            view.setCenter(ol.proj.transform(center, 'EPSG:4326', 'EPSG:3857'));
        }
    });

}(window.EMAP.Route = window.EMAP.Route || {}, jQuery));

(function (EMAP_Vector, $, undefined) {

    $.extend(true, EMAP_Vector, {
        init: function () {
            var view = EMAP.map.getView();

            this.options = {
                'featureProjection': view.getProjection(),
                'dataProjection': new ol.proj.Projection({ code: 'EPSG:4326' })
            };

            this.formatter = new ol.format.WKT();

            this.source = new ol.source.Vector();
            this.vector = new ol.layer.Vector({
                source: this.source,
                visible: true,
                style: new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: 'rgba(255, 0, 0, .3)'
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'rgba(255, 0, 0, 1)',
                        width: 2
                    })
                })
            });

            EMAP.map.addLayer(this.vector);
        },
        removeFeatures: function() {
            this.source.clear();
        },
        addFeatures: function (arr) {
            var features = [];

            if(arr && arr.length > 0) {
                for (var i = 0; i < arr.length; i++) {
                    features.push(this.formatter.readFeature(arr[i], this.options));
                }

                this.source.addFeatures(features);
            }
        }
    });

}(window.EMAP.Vector = window.EMAP.Vector || {}, jQuery));

(function (EMAP_Measure, $, undefined) {

    $.extend(true, EMAP_Measure, {
        wgs84Sphere: new ol.Sphere(6378137),
        sketch: null,
        helpTooltipElement: null,
        helpTooltip: null,
        measureTooltipElement: null,
        measureTooltip: null,
        draw: null, // global so we can remove it later
        continuePolygonMsg: EMAP.Localization.Click_to_continue_drawing_polygon,
        continueLineMsg: EMAP.Localization.Click_to_continue_drawing_line,
        m_tooltips: [],
        init: function() {
            this.source = new ol.source.Vector();
            this.vector = new ol.layer.Vector({
                source: this.source,
                visible: true,
                style: new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: 'rgba(255, 255, 255, 0.2)'
                    }),
                    stroke: new ol.style.Stroke({
                        color: '#ffcc33',
                        width: 2
                    }),
                    image: new ol.style.Circle({
                        radius: 7,
                        fill: new ol.style.Fill({
                            color: '#ffcc33'
                        })
                    })
                })
            });

            this.lineStringDraw = new ol.interaction.Draw({
                source: this.source,
                type: "LineString",
                style: new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: 'rgba(255, 255, 255, 0.2)'
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'rgba(0, 0, 0, 0.5)',
                        lineDash: [10, 10],
                        width: 2
                    }),
                    image: new ol.style.Circle({
                        radius: 5,
                        stroke: new ol.style.Stroke({
                            color: 'rgba(0, 0, 0, 0.7)'
                        }),
                        fill: new ol.style.Fill({
                            color: 'rgba(255, 255, 255, 0.2)'
                        })
                    })
                })
            });
            this.polygonDraw = new ol.interaction.Draw({
                source: this.source,
                type: "Polygon",
                style: new ol.style.Style({
                    fill: new ol.style.Fill({
                        color: 'rgba(255, 255, 255, 0.2)'
                    }),
                    stroke: new ol.style.Stroke({
                        color: 'rgba(0, 0, 0, 0.5)',
                        lineDash: [10, 10],
                        width: 2
                    }),
                    image: new ol.style.Circle({
                        radius: 5,
                        stroke: new ol.style.Stroke({
                            color: 'rgba(0, 0, 0, 0.7)'
                        }),
                        fill: new ol.style.Fill({
                            color: 'rgba(255, 255, 255, 0.2)'
                        })
                    })
                })
            });

            EMAP.map.addLayer(this.vector);
        },
        toggle: function(type) {
            
            if (!this.vector)
                this.init();

            var scope = angular.element('body').scope();

            EMAP.map.un('pointermove', this.pointerMoveHandler);
            EMAP.map.un('singleclick', this.pointerMoveHandler);

            EMAP.map.removeInteraction(this.lineStringDraw);
            EMAP.map.removeInteraction(this.polygonDraw);

            $.each(this.m_tooltips, function (id, tooltip) {
                if(!tooltip.temporary)
                    EMAP.map.removeOverlay(tooltip);
            });
            this.measureTooltip = null;
            this.helpTooltip = null;
            this.measureTooltipElement = null;
            this.helpTooltipElement = null;

            if (this.currentType == type) {
                if (this.vector.get('visible')) {
                    this.vector.setVisible(false);
                    this.vector.getSource().clear();
                    this.ON = false;

                    $.each(this.m_tooltips, function (id, tooltip) {
                        EMAP.map.removeOverlay(tooltip);
                    });

                    //!!! popups logic
                    if (scope) {
                        EMAP.map.on('singleclick', scope.contentScreen.onClick);
                    }
                    //!!!
                }
                else
                {
                    EMAP.map.on('pointermove', this.pointerMoveHandler);
                    EMAP.map.on('singleclick', this.pointerMoveHandler);

                    this.vector.setVisible(true);
                    this.addInteraction(type);
                    this.ON = true;

                    //!!! popups logic
                    if (scope) {
                        EMAP.map.un('singleclick', scope.contentScreen.onClick);
                    }
                    //!!!
                }
            }
            else
            {
                EMAP.map.on('pointermove', this.pointerMoveHandler);
                EMAP.map.on('singleclick', this.pointerMoveHandler);

                this.vector.setVisible(true);
                this.addInteraction(type);
                this.ON = true;

                //!!! popups logic
                if (scope) {
                    EMAP.map.un('singleclick', scope.contentScreen.onClick);
                }
                //!!!
            }

            this.currentType = type;
        },
        pointerMoveHandler: function (evt) {
            var me = EMAP.Measure;

            if (evt.dragging) {
                return;
            }

            if (!me.vector.get('visible'))
                return;

            var helpMsg = EMAP.Localization.Click_to_start_drawing;
            var tooltipCoord = evt.coordinate;

            if (me.sketch) {
                var output;
                var geom = (me.sketch.getGeometry());
                if (geom instanceof ol.geom.Polygon) {
                    output = me.formatArea(geom);
                    helpMsg = me.continuePolygonMsg;
                    tooltipCoord = geom.getInteriorPoint().getCoordinates();
                } else if (geom instanceof ol.geom.LineString) {
                    output = me.formatLength(geom);
                    helpMsg = me.continueLineMsg;
                    tooltipCoord = geom.getLastCoordinate();
                }
                me.measureTooltipElement.innerHTML = output;
                me.measureTooltip.setPosition(tooltipCoord);
            }

            me.helpTooltipElement.innerHTML = helpMsg;
            me.helpTooltip.setPosition(evt.coordinate);
        },
        addInteraction: function (type) {
            var me = this;

            var draw = null;
            if (type == "LineString")
                draw = this.lineStringDraw;
            else if (type == "Polygon")
                draw = this.polygonDraw;

            if (draw) {
                EMAP.map.addInteraction(draw);

                this.createMeasureTooltip();
                this.createHelpTooltip();

                draw.on('drawstart',
                    function (evt) {
                        // set sketch
                        me.sketch = evt.feature;
                    }, this);

                draw.on('drawend',
                    function (evt) {
                        me.pointerMoveHandler(evt);

                        me.measureTooltipElement.className = 'tooltip tooltip-static';
                        me.measureTooltip.setOffset([0, -7]);

                        // unset sketch
                        me.sketch = null;
                        // unset tooltip so that a new one can be created

                        me.measureTooltipElement = null;


                        me.measureTooltip.temporary = true;
                        me.createMeasureTooltip();
                    }, this);
            }
        },
        createHelpTooltip: function () {
            if (this.helpTooltipElement) {
                this.helpTooltipElement.parentNode.removeChild(this.helpTooltipElement);
            }
            this.helpTooltipElement = document.createElement('div');
            this.helpTooltipElement.className = 'tooltip';
            this.helpTooltip = new ol.Overlay({
                element: this.helpTooltipElement,
                offset: [15, 0],
                positioning: 'center-left'
            });
            EMAP.map.addOverlay(this.helpTooltip);
            this.m_tooltips.push(this.helpTooltip);
        },
        createMeasureTooltip: function () {
            if (this.measureTooltipElement) {
                this.measureTooltipElement.parentNode.removeChild(this.measureTooltipElement);
            }
            this.measureTooltipElement = document.createElement('div');
            this.measureTooltipElement.className = 'tooltip tooltip-measure';

            this.measureTooltip = new ol.Overlay({
                element: this.measureTooltipElement,
                offset: [0, -15],
                positioning: 'bottom-center'
            });
            EMAP.map.addOverlay(this.measureTooltip);
            this.m_tooltips.push(this.measureTooltip);
        },
        formatLength: function(line) {
            var length = Math.round(line.getLength() * 100) / 100;
            var output;
            if (length > 100) {
                output = (Math.round(length / 1000 * 100) / 100) +
                    ' ' + 'km';
            } else {
                output = (Math.round(length * 100) / 100) +
                    ' ' + 'm';
            }
            return output;
        },
        formatArea: function(polygon){
            var area = polygon.getArea();
            var output;
            if (area > 10000) {
                output = (Math.round(area / 1000000 * 100) / 100) +
                    ' ' + 'km<sup>2</sup>';
            } else {
                output = (Math.round(area * 100) / 100) +
                    ' ' + 'm<sup>2</sup>';
            }
            return output;
        }
    });

}(window.EMAP.Measure = window.EMAP.Measure || {}, jQuery));

