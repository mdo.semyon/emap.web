function searchServiceFactoryFunction($http, utils) {

	var factory = {};
	
	factory.getEstates = function (prefix) {
	    var estates = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/getEstates",
	        contentType: "application/json; charset=utf-8",
	        data: "{'prefix':'" + prefix + "'}",
	        dataType: "json"
	    }).then(function (resp) {
	        var data = JSON.parse(resp.data.d);

	        return data;
	    });

	    return estates;
	};
	factory.getTenantsByEstateId = function (estateId, prefix) {
	    var tenants = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/getTenantsByEstateId",
	        data: "{'estateId':'" + estateId + "', 'prefix':'" + prefix + "'}",
	        contentType: "application/json; charset=utf-8",
	        dataType: "json"
	    }).then(function (resp) {
	        var data = JSON.parse(resp.data.d);
	        return data;
	    });

		return tenants;
	};
	factory.getProductTypes = function (estateId, prefix) {
	    var productTypes = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/getProductTypes",
	        data: "{'estateId':'" + estateId + "', 'prefix':'" + prefix + "'}",
	        contentType: "application/json; charset=utf-8",
	        dataType: "json"
	    }).then(function (resp) {
	        var data = JSON.parse(resp.data.d);
	        return data;
	    });

	    return productTypes;
	};
	factory.getSubCatTenants = function (estateId, productType, prefix) {
	    var tenants = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/getSubCatTenants",
	        data: "{'estateId':'" + estateId + "', 'productType':'" + productType + "', 'prefix':'" + prefix + "'}",
	        contentType: "application/json; charset=utf-8",
	        dataType: "json"
	    }).then(function (resp) {
	        var data = JSON.parse(resp.data.d);
	        return data;
	    });

	    return tenants;
	};
	factory.getInvestmets = function (estateId, prefix) {
	    var investmets = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/getInvestmets",
	        data: "{'estateId':'" + estateId + "', 'prefix':'" + prefix + "'}",
	        contentType: "application/json; charset=utf-8",
	        dataType: "json"
	    }).then(function (resp) {
	        var data = JSON.parse(resp.data.d);
	        return data;
	    });

	    return investmets;
	};
	factory.getSubCatInvestments = function (estateId, investmentsOrigin, prefix) {
	    var tenants = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/getSubCatInvestments",
	        data: "{'estateId':'" + estateId + "', 'investmentsOrigin':'" + investmentsOrigin + "', 'prefix':'" + prefix + "'}",
	        contentType: "application/json; charset=utf-8",
	        dataType: "json"
	    }).then(function (resp) {
	        var data = JSON.parse(resp.data.d);
	        return data;
	    });

	    return tenants;
	};

	factory.getCompanyParcels = function (tenantId, estateId, prefix) {
	    if (!estateId)
	        estateId = 0;

	    var parcels = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/getCompanyParcels",
	        data: "{'tenantId':'" + tenantId + "', 'estateId': " + estateId + "}",
	        contentType: "application/json; charset=utf-8",
	        dataType: "json"
	    }).then(function (resp) {
	        var data = JSON.parse(resp.data.d);
	        return data;
	    });

	    return parcels;
	};

	factory.getSubCategoryParcels = function (estateId, classId, subCategory, prefix) {
	    var parcels = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/getSubCategoryParcels",
	        data: "{'estateId':'" + estateId + "','classId':'" + classId + "','subCategory':'" + subCategory + "','prefix':'" + prefix + "'}",
	        contentType: "application/json; charset=utf-8",
	        dataType: "json"
	    }).then(function (resp) {
	        var data = JSON.parse(resp.data.d);
	        return data;
	    });

	    return parcels;
	};

	factory.getFromRouteList = function () {
	    var places = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/getFromRouteList",
	        data: "{'prefix':'" + prefix + "'}",
	        contentType: "application/json; charset=utf-8",
	        dataType: "json"
	    }).then(function (resp) {
	        var data = JSON.parse(resp.data.d);
	        return data;
	    });

	    return places;
	};
	
	factory.getEsateDetails = function (estateId, prefix) {
	    var details = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/getEsateDetails",
	        data: "{'estateId':'" + estateId + "', 'prefix':'" + prefix + "'}",
	        contentType: "application/json; charset=utf-8",
	        dataType: "json"
	    }).then(function (resp) {
	        var data = JSON.parse(resp.data.d);
	        return data;
	    });

	    return details;
	};

    factory.getClassifications = function (prefix) {
	    var details = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/getClassifications",
	        data: "{'prefix':'" + prefix + "'}",
	        contentType: "application/json; charset=utf-8",
	        dataType: "json"
	    }).then(function (resp) {
	        var data = JSON.parse(resp.data.d);
	        return data;
	    });

	    return details;
	};

	factory.getLegends = function (classId, prefix) {
	    var details = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/getLegends",
	        data: "{'classId':'" + classId + "', 'prefix':'" + prefix + "'}",
	        contentType: "application/json; charset=utf-8",
	        dataType: "json"
	    }).then(function (resp) {
	        var data = JSON.parse(resp.data.d);
	        return data;
	    });

	    return details;
	};
    /*
	factory.sendRouteSMS = function (toLng, toLat, phone, tenant, estate) {
	    var res = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/sendRouteSMS",
	        data: "{'toLng':'" + toLng + "', 'toLat':'" + toLat + "', 'phone':'" + phone + "', 'tenant':'" + tenant + "', 'estate':'" + estate + "'}",
	        contentType: "application/json; charset=utf-8",
	        dataType: "json"
	    }).then(function (resp) {
	        return resp.data.d;
	    });

	    return res;
	};
    */
	factory.sendMessageSMS = function (phone, message_body) {
	    var res = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/sendMessageSMS",
	        data: "{'phone':'" + phone + "', 'message_body':'" + message_body + "'}",
	        contentType: "application/json; charset=utf-8",
	        dataType: "json"
	    }).then(function (resp) {
	        return resp.data.d;
	    });

	    return res;
	};

	factory.sendEmail = function (to, message_subject, message_body) {
	    var res = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/sendEmail",
	        data: "{'to':'" + to + "', 'message_subject':'" + message_subject + "', 'message_body':'" + message_body + "'}",
	        contentType: "application/json; charset=utf-8",
	        dataType: "json"
	    }).then(function (resp) {
	        return resp.data.d;
	    });

	    return res;
	};

	factory.getTenantByLonLat = function (lon, lat, estateId, prefix) {
	    var res = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/getTenantByLonLat",
	        data: "{'lon':'" + lon + "', 'lat':'" + lat + "', 'estateId': '" + estateId + "', 'prefix':'" + prefix + "'}",
	        contentType: "application/json; charset=utf-8",
	        dataType: "json"
	    }).then(function (resp) {
	        var data = JSON.parse(resp.data.d);

	        if (data.Area > 0)
	            data.Area = data.Area.toFixed(0);

	        return data;
	    });

	    return res;
	};

	factory.getFeatureInfo = function (url) {
	    var res = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/getFeatureInfo",
	        data: "{'url':'" + url + "'}",
	        contentType: "text/html; charset=utf-8",
	        dataType: "text"
	    }).then(function (resp) {
	        var data = resp.data.d;
	        return data;
	    });

	    return res;
	};

	factory.getTenantById = function (Id, prefix) {
	    var res = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/getTenantById",
	        data: "{'Id':'" + Id + "', 'prefix':'" + prefix + "'}",
	        contentType: "text/html; charset=utf-8",
	        dataType: "text"
	    }).then(function (resp) {
	        var data = JSON.parse(resp.data.d);
	        return data;
	    });

	    return res;
	};

	factory.getSettings = function (prefix) {
	    var settings = $http({
	        method: "POST",
	        url: "EmapWebService.asmx/getSettings",
	        contentType: "application/json; charset=utf-8",
	        data: "{'prefix':'" + prefix + "'}",
	        dataType: "json"
	    }).then(function (resp) {
	        var data = JSON.parse(resp.data.d);

	        return data;
	    });

	    return settings;
	};


	return factory;
}

angular.module('emap.search', [])
.factory('search', ['$http', searchServiceFactoryFunction]);

angular.module('directive.loading', [])
.directive('loading', ['$http', function ($http) {
    return {
        restrict: 'A',
        link: function (scope, elm, attrs) {
            scope.isLoading = function () {
                return $http.pendingRequests.length > 0;
            };

            scope.$watch(scope.isLoading, function (v) {
                if (v) {
                    elm.show();
                } else {
                    elm.hide();
                }
            });
        }
    };
}]);