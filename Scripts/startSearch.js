﻿function searchServiceFactoryFunction($http, utils) {

    var factory = {};

    factory.getSettings = function (prefix) {
        var settings = $http({
            method: "POST",
            url: "../../EmapWebService.asmx/getSettings",
            contentType: "application/json; charset=utf-8",
            data: "{'prefix':'" + prefix + "'}",
            dataType: "json"
        }).then(function (resp) {
            var data = JSON.parse(resp.data.d);

            return data;
        });

        return settings;
    };

    return factory;
}

angular.module('emap.search', [])
.factory('search', ['$http', searchServiceFactoryFunction]);