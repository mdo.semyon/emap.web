﻿var GIS = GIS ||
    {
        help: function () {
            //console.log('help');
            /*
            var scope = angular.element('body').scope();
            if (scope) {
                scope.contentScreen.showHelp();
            }
            */
            var win = window.open(location.href.replace("/#",'').replace('#','').replace("/en", '').replace("/ar", '') + "/" + EMAP.Localization.Culture + "/Help", '_blank');

            if (win)
                win.focus();

        },
        liveChat: function () {
            console.log('liveChat');
        },
        goHome: function () {

            document.cookie = 'skipStart_=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
            window.open(location.href.replace('#',''));

            /*
            var scope = angular.element('body').scope();
            if (scope) {
                scope.startScreen.goHome();

                $('[data-toggle="popover"]').each(function () {
                    //the 'is' for buttons that trigger popups
                    //the 'has' for icons within a button that triggers a popup
                    $(this).popover('hide');
                });
            }
            */
        },
        share: function (isRoute) {
            //console.log('share');

            var scope = angular.element('body').scope();
            if (scope) {
                if(isRoute){
                    var _msg = EMAP.Localization.Route_from + ' ';
                    _msg += scope.contentScreen.t_route.from.formatted_address;
                    _msg += ' ' + EMAP.Localization.to + ' ';
                    _msg += scope.contentScreen.t_route.to.formatted_address + '.';

                    var _address = location.href.replace('#','');
                    _address += '?eid=' + scope.contentScreen.selected_estate.Id;
                    if (scope.contentScreen.selected_tenant && scope.contentScreen.selected_tenant.tid > 0)
                        _address += '&tid=' + scope.contentScreen.selected_tenant.tid;

                    scope.share.tweet_text = _msg;
                    scope.share.tweet_address = _address;

                    // Remove existing iframe
                    $('#emap-share-twitter iframe').remove();
                    // Generate new markup
                    var tweetBtn = $('<a></a>')
                        .addClass('twitter-share-button')
                        .attr('href', 'http://twitter.com/share')
                        .attr('data-url', _address)
                        .attr('data-text', _msg);
                    $('#emap-share-twitter').append(tweetBtn);

                    window.setTimeout(function () {
                        twttr.widgets.load();
                    }, 10);
                    //twttr.widgets.load();
                }
                else
                {
                    //!!!
                    scope.share.tweet_text = EMAP.Settings.twitter_text;
                    scope.share.tweet_address = location.href.replace('#','');

                    // Remove existing iframe
                    $('#emap-share-twitter iframe').remove();
                    // Generate new markup
                    var tweetBtn = $('<a></a>')
                        .addClass('twitter-share-button')
                        .attr('href', 'http://twitter.com/share')
                        .attr('data-url', location.href.replace('#', ''))
                        .attr('data-text', EMAP.Settings.twitter_text);
                    $('#emap-share-twitter').append(tweetBtn);

                    window.setTimeout(function () {
                        twttr.widgets.load();
                    }, 10);
                    //twttr.widgets.load();
                    //!!!

                    //scope.share.smsBody = "Explore Industrial Estates by visiting map.peie.om.";
                }
                scope.$apply(function () {
                    scope.share.back();
                });
            }

            if (!GIS.social_initialized) {
                
                var script = document.createElement('script');
                script.async = true;
                script.src = document.location.protocol + '//connect.facebook.net/en_US/all.js#xfbml=1&appId=1664848010456718';
                document.getElementById('fb-root').appendChild(script);
                
                //window.setTimeout(function () {
                    //document.getElementById('social-buttons').removeAttribute('class');
                    //window.twttr.widgets.load(document.getElementById('emap-share-twitter'));
                //}, 1000);

                GIS.social_initialized = true;
            }

            jQuery('#emap-share-window').modal('show');
        },
        State: 0,
        BIT_ESTATES_LOADED: 1,
        BIT_CLASSIFICATIONS_LOADED: 2,
        BIT_TENANTS_LOADED: 4,
        Binary:
        {
            //bitwise operations helpers
            setBit: function (state, bit) {
                var b = state & bit;
                if (b == 0)
                    state = state | bit;

                return state;
            },
            setBits: function (state, bits) {
                var b;
                for (var i = 0; i < bits.length; i++) {
                    b = state & bits[i];
                    if (b == 0)
                        state = state | bits[i];
                }
                return state;
            },
            clearBit: function (state, bit) {
                state = state & ~bit;
                return state;
            },
            clearBits: function (state, bits) {
                for (var i = 0; i < bits.length; i++)
                    state = state & ~bits[i];
                return state;
            },
            checkBit: function (state, bit) {
                state = state & bit;
                return state != 0;
            },
            checkBits: function (state, bits) {
                var b;
                for (var i = 0; i < bits.length; i++) {
                    b = state & bits[i];
                    if (b == 0)
                        return false;
                }
                return true;
            }
        },
        getSearchParameters: function() {
            var prmstr = window.location.search.substr(1);
            return prmstr != null && prmstr != "" ? GIS.transformToAssocArray(prmstr) : {};
        },
        transformToAssocArray: function(prmstr) {
            var params = {};
            var prmarr = prmstr.split("&");
            for ( var i = 0; i < prmarr.length; i++) {
                var tmparr = prmarr[i].split("=");
                params[tmparr[0]] = parseInt(tmparr[1]);
            }
            return params;
        }
    };

function mainController($scope, $sce, search, $timeout) {
    GIS.params = GIS.getSearchParameters();

    $scope.isMobile = window.innerWidth < 768;
    $scope.prefix = EMAP.Localization.Culture;

    $scope.emapContextMenuStyle = "";

    $scope.startScreen = {
        start: function () {
            if ($scope.startScreen.skipStart_)
                document.cookie = "skipStart_=true;";

            $('#rs-start-screen').css('display', 'none');

            //!!! LiveChat
            if (!$scope.isMobile) {
                var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
                lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
            }
            //!!!

            search.getEstates($scope.prefix).then(function (res) {
                $scope.contentScreen.estates = res;

                var features = [];
                jQuery.each($scope.contentScreen.estates, function (idx, estate) {

                    var feature = new ol.Feature({
                        estateId: estate.Id,
                        geometry: new ol.geom.Point(ol.proj.transform([estate.lon, estate.lat], 'EPSG:4326', 'EPSG:3857'))
                    });
                    var iconStyle = new ol.style.Style({
                        image: new ol.style.Icon(({
                            anchor: [0.5, 0.5],
                            anchorXUnits: 'fraction',
                            anchorYUnits: 'pixels',
                            opacity: 0.75,
                            src: 'Images/marker.gif'
                        })),
                        text: estate.Id == 2 ? EMAP.createTextStyle("") : EMAP.createTextStyle(estate.EstateName)
                    });
                    feature.setStyle(iconStyle);
                    features.push(feature);
                });
                EMAP.iconsSource.addFeatures(features);
            });

            search.getClassifications($scope.prefix).then(function (res) {
                var classes = [];
                jQuery.each(res, function (id, item) {
                    classes.push(item);
                });

                $scope.contentScreen.classifications = classes;
            });

            $scope.EMAP.Vector.init();
            //!!! 28.12.2015
            $scope.EMAP.Route.init();
            //!!!

            $('#rs-content-screen').css('display', 'block');
            $('.ol-overlaycontainer-stopevent').css('display', 'block');
        },
        goHome: function() {
            $('#rs-content-screen').css('display', 'none');
            $('.ol-overlaycontainer-stopevent').css('display', 'none');
            search.getSettings($scope.prefix).then(function (res) {
                $scope.startScreen.settings = res;

                $scope.startScreen.settings.tabContent_1_html = $sce.trustAsHtml(res.tabContent_1);
                $scope.startScreen.settings.tabContent_2_html = $sce.trustAsHtml(res.tabContent_2);
                //$scope.startScreen.settings.tabContent_3_html = $sce.trustAsHtml(res.tabContent_3);
                $scope.startScreen.settings.tabContent_4_html = $sce.trustAsHtml(res.tabContent_4);
                //$scope.startScreen.settings.tabContent_5_html = $sce.trustAsHtml(res.tabContent_5);
                $scope.startScreen.settings.welcome_html = $sce.trustAsHtml(res.welcomeText);

                $('#emap-start-tab-3-selector').tab('show');
                $('.emap-start-screen-content-block').fadeIn('slow');
                //console.log('a');
            });
            $('#rs-start-screen').css('display', 'block');

            $('#rs-start-screen .emap-context-menu').css('display', 'block');
            $('#rs-start-screen .emap-logo').css('display', 'block');
        },
        getCookie: function(name) {
            var value = "; " + document.cookie;
            var parts = value.split("; " + name + "=");
            if (parts.length == 2) 
                return parts.pop().split(";").shift();
        },
        skipStart_: false,
        IsTabsVisivle: false,
        settings: {},
        skipStart: function () {
            if ($scope.startScreen.skipStart_)
                document.cookie = "skipStart_=true;";
            else
                document.cookie = "skipStart_=false;";
        }
    };

    $scope.share = {
        /* share buttons */
        tweet_text: EMAP.Settings.twitter_text,
        tweet_address: location.href.replace('#', ''),
        fb_share: function() {

            if ($scope.contentScreen.t_route.isCalculated &&
                $scope.contentScreen.selected_estate &&
                $scope.contentScreen.selected_tenant)
            {

                var _name = 'Route from ';
                _name += $scope.contentScreen.t_route.from.formatted_address;
                _name += ' to ';
                _name += $scope.contentScreen.t_route.to.formatted_address + '.';

                var _address = location.href.replace('#', '');

                _address += '?eid=' + $scope.contentScreen.selected_estate.Id;

                if ($scope.contentScreen.selected_tenant && $scope.contentScreen.selected_tenant.tid > 0)
                    _address += '&tid=' + $scope.contentScreen.selected_tenant.tid;

                //console.log($scope.contentScreen.selected_tenant);

                var _description = 'Destination: ';
                _description += ($scope.contentScreen.selected_tenant.formatted_address == 'Selected plot') ? 'Plot Number ' + $scope.contentScreen.selected_tenant.pnu : $scope.contentScreen.selected_tenant.formatted_address;
                _description += ' - ' + $scope.contentScreen.selected_estate.formatted_address;
                _description += ', Latitude: ' + $scope.contentScreen.t_route.to.lat;
                _description += ', Longitude: ' + $scope.contentScreen.t_route.to.lon;

                var _picture = EMAP.Settings.facebook_picture;

                FB.ui({
                    method: 'feed',
                    name: _name,
                    link: _address,
                    picture: _picture,
                    caption: 'map.peie.om',
                    description: _description,
                    message: "-message-"
                });
            }
            else
            {
                var _name = EMAP.Settings.facebook_name;
                var _address = location.href.replace('#','');
                var _description = EMAP.Settings.facebook_description;
                var _picture = EMAP.Settings.facebook_picture;

                FB.ui({
                    method: 'feed',
                    name: _name,
                    link: _address,
                    picture: _picture,
                    caption: 'map.peie.om',
                    description: _description,
                    message: "-message-"
                });
            }
        },
        /* end of share */
        isSocials: true,
        isRouteSMS: false,
        isMessageSMS: false,
        isSsmSuccess: false,
        isSmsError: false,
        smsErrorMessage: null,
        phone: null,
        isEmail: false,
        isEmailSuccess: false,
        isEmailError:false,
        //emailErrorMessage: null,
        //emailAddress: null,
        //!!! 06052016
        emailSubject: EMAP.Localization.Estate_Maping_Application,
        emailBody: EMAP.Localization.Explore_Industrial_Estates_by_visiting_map_peie_om,
        smsBody: EMAP.Localization.Explore_Industrial_Estates_by_visiting_map_peie_om,

        isBackVisible: false,
        isSendVisible: false,
        gotoRouteSms: function () {
            $scope.share.isBackVisible = true;
            $scope.share.isSendVisible = true;

            $scope.share.isSmsSuccess = false;
            $scope.share.isSmsError = false;
            $scope.share.smsErrorMessage = null;
            $scope.share.phone = null;

            $scope.share.isEmail = false;
            $scope.share.isMessageSMS = false;
            //$scope.share.isSocials = false;
            $('#social-buttons').css('display', 'none');
            $scope.share.isRouteSMS = true;
        },
        gotoMessageSms: function () {
            //$scope.$apply(function () {
            $scope.share.isBackVisible = true;
            $scope.share.isSendVisible = true;

            $scope.share.isSmsSuccess = false;
            $scope.share.isSmsError = false;
            $scope.share.smsErrorMessage = null;
            $scope.share.phone = null;

            $scope.share.isEmail = false;
            $scope.share.isRouteSMS = false;
            //$scope.share.isSocials = false;
            $('#social-buttons').css('display', 'none');
            $scope.share.isMessageSMS = true;
            //});
        },
        gotoEmail: function () {
            $scope.share.isBackVisible = true;
            $scope.share.isSendVisible = true;

            $scope.share.isEmailSuccess = false;
            $scope.share.isEmailError = false;
            $scope.share.emailErrorMessage = null;
            $scope.share.emailAddress = null;
            $scope.share.emailMessage = null;

            $scope.share.isRouteSMS = false;
            $scope.share.isMessageSMS = false;
            //$scope.share.isSocials = false;
            $('#social-buttons').css('display', 'none');
            $scope.share.isEmail = true;
        },
        back: function () {
            $scope.share.isBackVisible = false;
            $scope.share.isSendVisible = false;

            $scope.share.isRouteSMS = false;
            $scope.share.isMessageSMS = false;
            $scope.share.isEmail = false;
            //$scope.share.isSocials = true;
            $('#social-buttons').css('display', 'block');
        },
        send: function () {
            if ($scope.share.isRouteSMS) 
            {
                if (!$scope.share.phone) {
                    $scope.share.smsErrorMessage = EMAP.Localization.Phone_number_is_not_valid;
                    $scope.share.isSmsError = true;
                    $scope.share.isSmsSuccess = false;
                    return;
                }
                else {
                    $scope.share.isSmsError = false;
                    $scope.share.isSmsSuccess = false;
                }

                var tenant = $scope.contentScreen.selected_tenant.formatted_address;
                if ($scope.contentScreen.selected_tenant.formatted_address == 'Selected plot' || $scope.contentScreen.selected_tenant.formatted_address == 'الأرض المختارة')
                    tenant = EMAP.Localization.Plot_location_code + ": " + $scope.contentScreen.selected_tenant.fid + ", " + EMAP.Localization.Plot_area + ": " + $scope.contentScreen.selected_tenant.Area + " " + EMAP.Localization.m2 + "2";

                var smsBody = "";
                if (tenant) {
                    //smsBody = "Destination:" + tenant + "," + $scope.contentScreen.selected_estate.formatted_address;
                    smsBody += "Destination: " + tenant + ", " + $scope.contentScreen.selected_estate.formatted_address + " , Map Link: " + "http://maps.google.com/maps?q=" + $scope.contentScreen.t_route.to.lat + "," + $scope.contentScreen.t_route.to.lon;
                }
                else {
                    //smsBody = "Destination:" + $scope.contentScreen.selected_estate.formatted_address;
                    smsBody += "Destination: " + $scope.contentScreen.selected_estate.formatted_address + ", Map Link: " + "http://maps.google.com/maps?q=" + $scope.contentScreen.t_route.to.lat + "," + $scope.contentScreen.t_route.to.lon;
                }


                //var tenant = $scope.contentScreen.t_route.to.IsEstate ? "" : $scope.contentScreen.t_route.to.formatted_address;
                //$scope.contentScreen.loadingIndicator.promise = search.sendRouteSMS($scope.contentScreen.t_route.to.lon, $scope.contentScreen.t_route.to.lat, $scope.share.phone, tenant, $scope.contentScreen.selected_estate.formatted_address).then(function (error) {
                $scope.contentScreen.loadingIndicator.promise = search.sendMessageSMS($scope.share.phone, smsBody).then(function (error) {
                    if (error && error.length > 0) {
                        $scope.share.smsErrorMessage = error;
                        $scope.share.isSmsError = true;
                    }
                    else {
                        $scope.share.isSmsSuccess = true;
                    }
                });
            }
            if ($scope.share.isMessageSMS) {
                if (!$scope.share.phone) {
                    $scope.share.smsErrorMessage = EMAP.Localization.Phone_number_is_not_valid;
                    $scope.share.isSmsError = true;
                    $scope.share.isSmsSuccess = false;
                    return;
                }
                else {
                    $scope.share.isSmsError = false;
                    $scope.share.isSmsSuccess = false;
                }
                $scope.contentScreen.loadingIndicator.promise = search.sendMessageSMS($scope.share.phone, $scope.share.smsBody).then(function (error) {
                    if (error && error.length > 0) {
                        $scope.share.smsErrorMessage = error;
                        $scope.share.isSmsError = true;
                    }
                    else {
                        $scope.share.isSmsSuccess = true;
                    }
                });
            }
            else if ($scope.share.isEmail)
            {
                if (!$scope.share.emailAddress) {
                    $scope.share.emailErrorMessage = EMAP.Localization.Email_is_not_valid;
                    $scope.share.isEmailError = true;
                    $scope.share.isEmailSuccess = false;
                    return;
                }
                else {
                    $scope.share.isEmailError = false;
                    $scope.share.isEmailSuccess = false;
                }

                $scope.contentScreen.loadingIndicator.promise = search.sendEmail($scope.share.emailAddress, $scope.share.emailSubject, $scope.share.emailBody).then(function (error) {
                    if (error && error.length > 0) {
                        $scope.share.emailErrorMessage = error;
                        $scope.share.isEmailError = true;
                    }
                    else {
                        $scope.share.isEmailSuccess = true;
                    }
                });
            }
        },
    };
    //console.log($scope.share.smsBody);

    $scope.contentScreen = {
        estatesDropdownStyle: (EMAP.Localization.Culture == 'en') ? { 'text-align': 'left' } : { 'text-align': 'right' },
        tenantsDropdownStyle: (EMAP.Localization.Culture == 'en') ? { 'text-align': 'left' } : { 'text-align': 'right' },
        loadingIndicator: {
            delay: 0,
            minDuration: 0,
            message: 'Please Wait...',
            backdrop: true,
            promise: null
        },
        showHelp: function (id) {
            search.getSettings($scope.prefix).then(function (res) {
                $scope.contentScreen.help_html = $sce.trustAsHtml(res.help);
                jQuery('#emap-help-window').modal('show');
            });
        },
        showEstateDetails: function (id) {
            search.getEsateDetails($scope.contentScreen.selected_estate.Id, $scope.prefix).then(function (res) {
                $scope.contentScreen.estateDetails = res;

                //!!!
                if (!$scope.isMobile) {
                    if ($scope.prefix == 'en') {
                        var h = window.innerHeight - 10;

                        if ($scope.contentScreen.showLegendPopup)
                            h = h - $('.emap-legend-popup').height() - 10 - 5;

                        $('#emap-estate-info-en').height(h);
                    }
                    else if ($scope.prefix == 'ar') {
                        var h = window.innerHeight - 10;

                        if ($scope.contentScreen.showLegendPopup)
                            h = h - $('.emap-legend-popup').height() - 35 - 5;

                        $('#emap-estate-info-ar').height(h);
                    }
                }
                //!!!

                $scope.contentScreen.showEstateDetailsWindow = true;
                //jQuery('#emap-info-window').modal('show');

            });
        },
        hideEstateDetails: function() {
            $scope.contentScreen.showEstateDetailsWindow = false;
        },
        showTenantDetails: function (Id) {
            //var win = window.open('https://ems.peie.om/newDir/TenantDetails.aspx?lang=en-US&tid=' + Id, '_blank');

            //!!! 06052016
            var win = null;
            if (EMAP.Localization.Culture == 'ar')
                win = window.open(EMAP.Settings.companyProfileURL + Id + "&lang=ar-OM", '_blank');
            else
                win = window.open(EMAP.Settings.companyProfileURL + Id, '_blank');

            if(win)
                win.focus();
        },
        showTenantParcels: function (Id, estateId) {

            $scope.contentScreen.selected_tenant.tid = Id;

            search.getTenantById(Id, $scope.prefix).then(function (res) {
                $scope.contentScreen.selected_tenant.Count = res[0].Count;
                $scope.contentScreen.selected_tenant.Landuse = res[0].Landuse;
            });

            search.getCompanyParcels(Id, estateId).then(function (parcels) {
                $scope.EMAP.Vector.removeFeatures();
                $scope.EMAP.Vector.addFeatures(parcels);

                //!!!
                var ext = EMAP.Vector.source.getExtent();
                var v = EMAP.map.getView();
                v.fit(ext, EMAP.map.getSize());
                if (v.getZoom() > 17)
                    v.setZoom(17);
                //!!!

                //prepare routes
                var lonlat = ol.proj.transform(v.getCenter(), 'EPSG:3857', 'EPSG:4326');
                $scope.contentScreen.selected_tenant.lon = lonlat[0];
                $scope.contentScreen.selected_tenant.lat = lonlat[1];
                $scope.contentScreen.selected_tenant.formatted_address = $scope.contentScreen.selected_tenant.Name;
                $scope.contentScreen.t_route.to = {
                    lat: $scope.contentScreen.selected_tenant.lat,
                    lon: $scope.contentScreen.selected_tenant.lon,
                    formatted_address: $scope.contentScreen.selected_tenant.formatted_address
                };
            });
            $scope.contentScreen.showTenantTab = true;
            $('#emap-sidebar-tenant-tab-selector').tab('show');
        },
        onEstateSelected: function (estateId) {
            search.getTenantsByEstateId(estateId, $scope.prefix).then(function (tenants) {
                jQuery.each($scope.contentScreen.estates, function (id, estate) {
                    if (estate.Id == estateId) {

                        if (!estate.tenants)
                            estate.tenants = tenants;

                        $scope.contentScreen.selected_estate = estate;
                        //!!!
                        $scope.contentScreen.clearClassification();
                        //!!!

                        $(".emap-tenants-main-search").val($scope.contentScreen.selected_estate.formatted_address);
                        $(".emap-tenants-search").val("");
                        if ($scope.isMobile) {
                            $("#emap-estates-search-group").css('width', '35%');
                            $("#emap-estates-search-group input").css('width', '60%');
                        }
                        else {
                            $("#emap-estates-search-group").css('width', '170px');
                            $("#emap-estates-search-group input").css('width', '116px');
                        }

                    
                        $scope.contentScreen.dropdownEstateClass = "selected";

                        EMAP.Wms.hideIcons();
                    }
                });

                $scope.contentScreen.t_route.to = $scope.contentScreen.selected_estate;
                GIS.State = GIS.Binary.setBit(GIS.State, GIS.BIT_TENANTS_LOADED);
                if (GIS.params.tid > 0 && GIS.Binary.checkBits(GIS.State, [GIS.BIT_ESTATES_LOADED, GIS.BIT_CLASSIFICATIONS_LOADED, GIS.BIT_TENANTS_LOADED])) {
                    $scope.contentScreen.onTenantChanged(GIS.params.tid);
                }
            });

            $scope.contentScreen.selected_tenant = null;
            $scope.contentScreen.selected_tenants = null;
            $scope.contentScreen.selected_classification = null;
            $scope.contentScreen.selected_type = null;
            $scope.contentScreen.selected_country = null;
            $scope.contentScreen.showLegendPopup = false;
            $scope.contentScreen.hideEstateDetails();

            $scope.contentScreen.t_clearRoute();
        },
        onTenantChanged: function (Id) {
            $scope.contentScreen.selected_tenants = null;

            if (Id == -1)
                return;

            jQuery.each($scope.contentScreen.selected_estate.tenants, function (idx, tenant) {
                if (tenant.Id == Id) {

                    $scope.contentScreen.selected_tenant = tenant;
                    $scope.contentScreen.showTenantParcels($scope.contentScreen.selected_tenant.Id);

                    $(".emap-tenants-search").val(tenant.Name);
                    $scope.contentScreen.dropdownTenantClass = "selected";
                }
            });
            $scope.contentScreen.hideEstateDetails();

            $scope.contentScreen.t_clearRoute();
        },
        //!!!
        clearEstate: function () {
            $scope.contentScreen.selected_estate = null;

            $('#emap-estates-search-group').css('width', '100%');
            $('.emap-tenants-main-search').css('width', '100%');
            $('.emap-tenants-main-search').val("");
            $scope.contentScreen.dropdownEstateClass = "";

            $scope.contentScreen.selected_tenant = null;
            $scope.contentScreen.selected_tenants = null;
            $scope.contentScreen.tenantForEstate = null;
            $('.emap-tenants-search').val("");
            $scope.contentScreen.selected_type = null;
            $scope.contentScreen.selected_country = null;

            $scope.contentScreen.t_clearRoute();
            $scope.contentScreen.setCenter();

            $scope.contentScreen.showLegendPopup = false;
            $scope.contentScreen.showEstateDetailsWindow = false;

            EMAP.Wms.showIcons();
       },
        clearTenant: function () {
            $scope.contentScreen.selected_tenant = null;
            $scope.contentScreen.selected_tenants = null;
            $scope.contentScreen.tenantForEstate = null;
            $('.emap-tenants-search').val("");
            $scope.contentScreen.dropdownTenantClass = "";

            //$scope.contentScreen.onClassificationChanged($scope.contentScreen.classifications[0]);
            //$scope.contentScreen.selected_type = null;
            //$scope.contentScreen.selected_country = null;

            $scope.contentScreen.t_clearRoute();

            //EMAP.Wms.showlayer($scope.contentScreen.selected_estate.Id);
            EMAP.Wms.fitToLayerExtentByID($scope.contentScreen.selected_estate.Id);

            EMAP.Vector.removeFeatures();

            //if ($scope.isMobile) {
            //    $scope.emapContextMenuStyle = { 'top': '110px', 'margin': '0 5px 0 0', 'width': 'auto' };
            //    $scope.emapLogoStyle = { 'top': '110px' };
            //}
        },
        clearClassification: function () {
            $scope.contentScreen.onClassificationChanged($scope.contentScreen.classifications[1]);
            $scope.contentScreen.selected_type = null;
            $scope.contentScreen.selected_country = null;
        },
        onClassificationClick: function () {
            $('#emap-classification-dropdown button').dropdown('emap_toggle');
        },
        onCountryClick: function () {
            $('#emap-country-dropdown button').dropdown('emap_toggle');
        },
        onProductTypeClick: function () {
            $('#emap-product-types-dropdown button').dropdown('emap_toggle');
        },
        //----------------------------------------Map section---------------------------------------
        setCenter: function() {
            var view = EMAP.map.getView();
            view.setCenter([6132795.443472614, 2442274.4548305715]);
            view.setZoom(6);
        },
        //----------------------------------------Routes section------------------------------------
        routeGuideClass: 'icon-pageforward',
        routeClearClass: 'icon-share-alt',
        geolocation: null,
        t_route: {
            from: null,
            to: null,
            isCalculated: false,
            //isError: false,
            //isSuccess: false,
            //errorMessage: "",
            //phone: '1234',
            geolocationClass: 'icon-gpsoff-gps'
        },
        t_geolocate: function () {
            //$scope.tryAPIGeolocation();

            
            jQuery.post("https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyC9Pn0GP3LyqmTxUXsOOvtku3775XAgyDA",
                function (success) {

                    $scope.contentScreen.t_route.from = {
                        lat: success.location.lat,
                        lon: success.location.lng,
                        formatted_address: (EMAP.Localization.Culture == 'en') ? EMAP.Constants.from_arr[0].formatted_address_en : EMAP.Constants.from_arr[0].formatted_address_ar
                    };

                    $scope.contentScreen.t_route.geolocationClass = 'icon-gpson';
                }
          )
          .fail(function (err) {
              $scope.contentScreen.geolocation = null;
          });
            


            /*
            if ($scope.contentScreen.geolocation) {
                $scope.contentScreen.t_route.from = {
                    lat: $scope.contentScreen.geolocation.lat,
                    lon: $scope.contentScreen.geolocation.lon,
                    formatted_address: $scope.contentScreen.geolocation.formatted_address
                };
                $scope.contentScreen.t_route.geolocationClass = 'icon-gpson';

                var formatted_address = (EMAP.Localization.Culture == 'en') ? EMAP.Constants.from_arr[0].formatted_address_en : EMAP.Constants.from_arr[0].formatted_address_ar;
                $('#emap-route-from').val(formatted_address);

                return null;
            }
            else {
                return 'Unable to detect user current location/no geolocation support';
            }
            */
        },
        t_guideMe: function () {
            $scope.EMAP.Route.t_findRoute($scope.contentScreen.t_route.from, $scope.contentScreen.t_route.to);

            $scope.contentScreen.showLegendPopup = false;
            $scope.contentScreen.t_route.isCalculated = true;
            $scope.contentScreen.showEstateDetailsWindow = false;
        },
        t_routeFromPoint: function (lon, lat, tenantId) {
            $scope.contentScreen.t_route.from = { lon: lon, lat: lat };

            $scope.contentScreen.$apply(function () {
                $scope.contentScreen.t_geolocate();

                if ($scope.contentScreen.t_route.to) {
                    $scope.EMAP.Route.t_findRoute($scope.contentScreen.t_route.from, $scope.contentScreen.t_route.to);
                    $scope.contentScreen.t_route.isCalculated = true;
                }
                else {
                    $scope.contentScreen.t_route.isCalculated = false;
                }
            });

            jQuery.each($scope.contentScreen.selected_estate.tenants, function (id, item) {
                if (item.Id == tenantId) {
                    $('.emap-sidebar-t_route-from input').val(item.Name);
                }
            });
        },
        //t_route: { isCalculated: false },
        t_clearRoute: function () {
            EMAP.Route.routeSource.clear();
            $("#emap-t_route-directions").html('');

            $scope.contentScreen.t_route.to = $scope.contentScreen.selected_tenant;
            $scope.contentScreen.t_route.isCalculated = false;
            $scope.contentScreen.showLegendPopup = true;

            if (EMAP.Vector.source) {
                var ext = EMAP.Vector.source.getExtent();
                var v = EMAP.map.getView();
                v.fit(ext, EMAP.map.getSize());
                if (v.getZoom() > 17)
                    v.setZoom(17);
            }
        },
        //!!! 06052016

        //classification
        classifications: [],
        selected_classification: {},
        selected_type: {},
        selected_country: {},
        onClassificationChanged: function (c) {
            $scope.EMAP.Vector.removeFeatures();

            /*
            $scope.contentScreen.selected_classId = c.Id;
            if ($scope.contentScreen.selected_classId >= 0)
                $scope.contentScreen.selected_classId = parseInt($scope.contentScreen.selected_classId);
            else
                return;
            */
            if (c.Id >= 0)
                $scope.contentScreen.selected_classId = parseInt(c.Id);
            else
                return;

            $scope.contentScreen.selected_classification = {};
            $scope.contentScreen.selected_classification['classId'] = $scope.contentScreen.selected_classId;
            $scope.contentScreen.selected_classification['Name'] = c.Name;

            switch ($scope.contentScreen.selected_classId) {
                /*
                case EMAP.Enums.Default:
                    $scope.EMAP.Wms.showlayer($scope.contentScreen.selected_estate.Id);
                    $scope.contentScreen.showLegendPopup = false;
                    break;
                */
                case EMAP.Enums.Vacancy:
                    $scope.contentScreen.showLegendPopup = true;
                    search.getLegends($scope.contentScreen.selected_classId, $scope.prefix).then(function (res) {
                        var legends = jQuery.map(res, function (item) {
                            return { style: "{'background-color':'" + item.Color + "'}", name: item.Name }
                        });

                        $scope.contentScreen.selected_classification.legends = legends;
                        //$scope.EMAP.Wms.addClassificationLayer($scope.contentScreen.selected_classId, $scope.contentScreen.selected_estate.Id);
                        $scope.EMAP.Wms.showlayer($scope.contentScreen.selected_estate.Id);
                    });

                    //$scope.EMAP.Wms.showlayer($scope.contentScreen.selected_estate.Id);
                    //$scope.contentScreen.showLegendPopup = false;
                    break;
                case EMAP.Enums.LandUse:
                case EMAP.Enums.TenantStage:
                case EMAP.Enums.OwnershipType:
                    $scope.contentScreen.showLegendPopup = true;
                    search.getLegends($scope.contentScreen.selected_classId, $scope.prefix).then(function (res) {
                        var legends = jQuery.map(res, function (item) {
                            return { style: "{'background-color':'" + item.Color + "'}", name: item.Name }
                        });

                        $scope.contentScreen.selected_classification.legends = legends;
                        $scope.EMAP.Wms.addClassificationLayer($scope.contentScreen.selected_classId, $scope.contentScreen.selected_estate.Id);
                    });
                    break;
                case EMAP.Enums.ProductType:
                    $scope.contentScreen.showLegendPopup = false;
                    if (!$scope.contentScreen.selected_estate)
                        return;

                    search.getProductTypes($scope.contentScreen.selected_estate.Id, $scope.prefix).then(function (types) {
                        $scope.contentScreen.selected_classification.types = types;
                    });

                    break;
                case EMAP.Enums.Investment:
                    $scope.contentScreen.showLegendPopup = false;
                    if (!$scope.contentScreen.selected_estate)
                        return;

                    search.getInvestmets($scope.contentScreen.selected_estate.Id, $scope.prefix).then(function (countries) {
                        $scope.contentScreen.selected_classification.countries = countries;
                    });
                    break;
                default:
                    break;
            }
            $scope.contentScreen.selected_type = null;
            $scope.contentScreen.selected_country = null;
        },
        onProductTypeChanged: function(t) {
            if (!$scope.contentScreen.selected_classification.types)
                return;

            $scope.contentScreen.selected_type = t;
            $scope.contentScreen.showLegendPopup = true;

            search.getSubCatTenants($scope.contentScreen.selected_estate.Id, $scope.contentScreen.selected_type.Name, $scope.prefix).then(function (tenants) {
                $scope.contentScreen.selected_type.tenants = tenants;
            });

            search.getSubCategoryParcels($scope.contentScreen.selected_estate.Id, $scope.contentScreen.selected_classification.classId, $scope.contentScreen.selected_type.Name, $scope.prefix).then(function (parcels) {
                $scope.EMAP.Vector.removeFeatures();
                $scope.EMAP.Vector.addFeatures(parcels);
            });
        },
        onCountryChanged: function (c) {
            if (!$scope.contentScreen.selected_classification.countries)
                return;

            $scope.contentScreen.selected_country = c;
            $scope.contentScreen.showLegendPopup = true;

            search.getSubCatInvestments($scope.contentScreen.selected_estate.Id, $scope.contentScreen.selected_country.Name, $scope.prefix).then(function (tenants) {
                $scope.contentScreen.selected_country.tenants = tenants;
            });

            search.getSubCategoryParcels($scope.contentScreen.selected_estate.Id, $scope.contentScreen.selected_classification.classId, $scope.contentScreen.selected_country.Name, $scope.prefix).then(function (parcels) {
                $scope.EMAP.Vector.removeFeatures();
                $scope.EMAP.Vector.addFeatures(parcels);
            });
        },
        clearClassificationCountry: function () {
            $scope.contentScreen.selected_coountry = null;
            $scope.contentScreen.showLegendPopup = false;
            $scope.EMAP.Vector.removeFeatures();
        },
        clearClassificationProductType: function () {
            $scope.contentScreen.selected_type = null;
            $scope.contentScreen.showLegendPopup = false;
            $scope.EMAP.Vector.removeFeatures();
        },
        //popups
        showLegendPopup: false,
        onClick: function (evt) {
            var lonlat = ol.proj.transform(evt.coordinate, 'EPSG:3857', 'EPSG:4326');

            if ($scope.contentScreen.selected_estate) {
                var xy = new Array(2),
                    zone = null;


                if ($scope.contentScreen.selected_estate.Id == 4 || $scope.contentScreen.selected_estate.Id == 9)
                {
                    zone = EMAP.Helper.LatLonToUTMXY(EMAP.Helper.DegToRad(lonlat[1]), EMAP.Helper.DegToRad(lonlat[0]), 39, xy);
                }
                else if ($scope.contentScreen.selected_estate.Id == 3 || $scope.contentScreen.selected_estate.Id == 8 || $scope.contentScreen.selected_estate.Id == 10)
                {
                    zone = EMAP.Helper.LatLonToUTMXY(EMAP.Helper.DegToRad(lonlat[1]), EMAP.Helper.DegToRad(lonlat[0]), 40, xy);
                }
                else
                {
                    zone = EMAP.Helper.LatLonToUTMXY(EMAP.Helper.DegToRad(lonlat[1]), EMAP.Helper.DegToRad(lonlat[0]), 40, xy);
                    xy[0] -= 289.558;
                    xy[1] -= 249.45;
                }
                search.getTenantByLonLat(xy[0], xy[1], $scope.contentScreen.selected_estate.Id, $scope.prefix).then(function (lonlatRes) {


                    if (lonlatRes && lonlatRes.fid > 0) {

                        if (lonlatRes.Tenants && lonlatRes.Tenants.length > 0)
                        {
                            $scope.contentScreen.selected_tenants = lonlatRes.Tenants;
                            $scope.contentScreen.selected_tenant = lonlatRes.Tenants[0];
                            $scope.contentScreen.selected_tenant.tid = lonlatRes.Tenants[0].Id;
                            $scope.contentScreen.selected_tenant.formatted_address = lonlatRes.Tenants[0].Name;

                            $(".emap-tentants-search input").val($scope.contentScreen.selected_tenant.Name);
                        }
                        else
                        {
                            $scope.contentScreen.selected_tenants = null;
                            $scope.contentScreen.selected_tenant = {
                                formatted_address: EMAP.Localization.Selected_plot,
                                Name: EMAP.Localization.Selected_plot,
                                Area: lonlatRes.Area,
                                fid: lonlatRes.fid,
                                Pnu: lonlatRes.Pnu,
                                Landuse: lonlatRes.Landuse,
                                Invest: lonlatRes.Invest
                            }

                            $(".emap-tentants-search input").val('');
                        }

                        $scope.EMAP.Vector.removeFeatures();
                        if (lonlatRes.wkt) {
                            $scope.EMAP.Vector.addFeatures([lonlatRes.wkt]);
                            //!!!
                            var ext = EMAP.Vector.source.getExtent();
                            var v = EMAP.map.getView();
                            v.fit(ext, EMAP.map.getSize());
                            if (v.getZoom() > 17)
                                v.setZoom(17);
                            //!!!

                            //prepare routes
                            var lonlat = ol.proj.transform(v.getCenter(), 'EPSG:3857', 'EPSG:4326');
                            $scope.contentScreen.selected_tenant.lon = lonlat[0];
                            $scope.contentScreen.selected_tenant.lat = lonlat[1];
                            $scope.contentScreen.t_route.to = {
                                lat: $scope.contentScreen.selected_tenant.lat,
                                lon: $scope.contentScreen.selected_tenant.lon,
                                formatted_address: $scope.contentScreen.selected_tenant.formatted_address
                            };
                            $('.emap-tenants-search').val($scope.contentScreen.selected_tenant.formatted_address);
                            $scope.contentScreen.dropdownTenantClass = "selected";
                            //!!!
                        }
                    }
                
                    //console.log(lonlatRes);

                    $scope.contentScreen.showTenantTab = true;
                    $('#emap-sidebar-tenant-tab-selector').tab('show');
                });
            }
        
            var feature = $scope.EMAP.map.forEachFeatureAtPixel(evt.pixel,
                function (feature, layer) {
                    return feature;
                });
            if (feature) {
                if (feature.get('estateId') > 0) {

                    var estateId = feature.get('estateId');

                    search.getTenantsByEstateId(estateId, $scope.prefix).then(function (tenants) {
                        jQuery.each($scope.contentScreen.estates, function (id, estate) {
                            if (estate.Id == estateId) {
                                $scope.contentScreen.onEstateSelected(estateId);
                            }
                        });

                        $scope.contentScreen.t_route.to = $scope.contentScreen.selected_estate;
                    });

                    $scope.contentScreen.selected_tenant = null;
                    $scope.contentScreen.selected_classification = null;
                    $scope.contentScreen.selected_type = null;
                    $scope.contentScreen.selected_country = null;
                    $scope.contentScreen.showLegendPopup = false;
                }
            }
        }
    };


    $scope.$on('g-places-autocomplete:select.tenantsForEstate', function (event, data) {
        //console.log('g-places-autocomplete:select.tenantsForEstate');

        if (data && data.place) {
            search.getTenantsByEstateId(data.place.EstateId, $scope.prefix).then(function (tenants) {
                jQuery.each($scope.contentScreen.estates, function (id, estate) {
                    if (estate.Id == data.place.EstateId) {

                        if (!estate.tenants)
                            estate.tenants = tenants;

                        $scope.contentScreen.selected_estate = estate;
                        $scope.EMAP.Wms.showlayer(estate.Id);
                    }
                });
                jQuery.each($scope.contentScreen.selected_estate.tenants, function (id, tenant) {
                    if (tenant.Id == data.place.Id) {

                        $scope.contentScreen.selected_tenant = tenant;
                        $scope.contentScreen.showTenantParcels($scope.contentScreen.selected_tenant.Id);
                    }
                });
            });
        }
    });
    $scope.$on('g-places-autocomplete:select.tenants', function (event, data) {
        //console.log('g-places-autocomplete:select.tenants', data.place);

        if (data && data.place) {
            search.getTenantsByEstateId(data.place.EstateId, $scope.prefix).then(function (tenants) {
                jQuery.each($scope.contentScreen.estates, function (id, estate) {
                    if (estate.Id == data.place.EstateId) {

                        if (!estate.tenants)
                            estate.tenants = tenants;

                        $scope.contentScreen.selected_estate = estate;
                        $scope.EMAP.Wms.showlayer(estate.Id);

                        $(".emap-tenants-main-search").val($scope.contentScreen.selected_estate.formatted_address);

                        if ($scope.isMobile) {
                            $("#emap-estates-search-group").css('width', '35%');
                            $("#emap-estates-search-group input").css('width', '60%');
                        } else {
                            $("#emap-estates-search-group").css('width', '170px');
                            $("#emap-estates-search-group input").css('width', '116px');
                        }
                        $scope.contentScreen.dropdownEstateClass = "selected";
                    }
                });
                jQuery.each($scope.contentScreen.selected_estate.tenants, function (id, tenant) {
                    if (tenant.Id == data.place.Id) {

                        $scope.contentScreen.selected_tenant = tenant;
                        //!!! 31.03.16
                        $scope.contentScreen.showTenantParcels(data.place.Id, data.place.EstateId);
                        //!!!

                        $(".emap-tenants-search").val(tenant.Name);
                        $scope.contentScreen.dropdownTenantClass = "selected";
                    }
                });

                $scope.contentScreen.t_route.to = $scope.contentScreen.selected_estate;
            });
        }

        $scope.contentScreen.selected_classification = null;
        $scope.contentScreen.selected_type = null;
        $scope.contentScreen.selected_country = null;
        $scope.contentScreen.showLegendPopup = false;
    });
    $scope.$on('g-places-autocomplete:select.t_routeFrom', function (event, data) {
        //console.log('g-places-autocomplete:select.t_routeFrom');

        if (data.place.id == 0)
        {
            $scope.contentScreen.t_route.from = {
                lat: $scope.contentScreen.geolocation.lat,
                lon: $scope.contentScreen.geolocation.lon,
                formatted_address: (EMAP.Localization.Culture == 'en') ? EMAP.Constants.from_arr[0].formatted_address_en : EMAP.Constants.from_arr[0].formatted_address_ar //"Current Location"
            };
        }
        else
        {
            $scope.contentScreen.t_route.from = data.place;
        }
    });
    $scope.$on('g-places-autocomplete:select.classificationCountry', function (event, data) {
        if (data && data.place) {

            if (!$scope.contentScreen.selected_classification.countries)
                return;

            jQuery.each($scope.contentScreen.selected_classification.countries, function (id, c) {
                if (c.Name == data.place.formatted_address) {

                    $scope.contentScreen.selected_country = c;
                    $scope.contentScreen.showLegendPopup = true;

                    search.getSubCatInvestments($scope.contentScreen.selected_estate.Id, $scope.contentScreen.selected_country.Name, $scope.prefix).then(function (tenants) {
                        $scope.contentScreen.selected_country.tenants = tenants;
                    });

                    search.getSubCategoryParcels($scope.contentScreen.selected_estate.Id, $scope.contentScreen.selected_classification.classId, $scope.contentScreen.selected_country.Name, $scope.prefix).then(function (parcels) {
                        $scope.EMAP.Vector.removeFeatures();
                        $scope.EMAP.Vector.addFeatures(parcels);
                    });
                }
            });
        }
    });
    $scope.$on('g-places-autocomplete:select.classificationProductType', function (event, data) {
        if (data && data.place) {

            if (!$scope.contentScreen.selected_classification.types)
                return;

            jQuery.each($scope.contentScreen.selected_classification.types, function (id, t) {
                if (t.Name == data.place.formatted_address) {

                    $scope.contentScreen.selected_type = t;
                    $scope.contentScreen.showLegendPopup = true;

                    search.getSubCatTenants($scope.contentScreen.selected_estate.Id, $scope.contentScreen.selected_type.Name, $scope.prefix).then(function (tenants) {
                        $scope.contentScreen.selected_type.tenants = tenants;
                    });

                    search.getSubCategoryParcels($scope.contentScreen.selected_estate.Id, $scope.contentScreen.selected_classification.classId, $scope.contentScreen.selected_type.Name, $scope.prefix).then(function (parcels) {
                        $scope.EMAP.Vector.removeFeatures();
                        $scope.EMAP.Vector.addFeatures(parcels);
                    });
                }
            });
        }
    });

    /*
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
            function (position) {
                
                $scope.contentScreen.geolocation = {
                    lat: position.coords.latitude,
                    lon: position.coords.longitude,
                    formatted_address: (EMAP.Localization.Culture == 'en') ? EMAP.Constants.from_arr[0].formatted_address_en : EMAP.Constants.from_arr[0].formatted_address_ar // "Current Location" //EMAP.Localization.lon + position.coords.longitude.toFixed(6) + ', ' + EMAP.Localization.lat + position.coords.latitude.toFixed(6)
                };
                
                $scope.contentScreen.t_route.geolocationClass = 'icon-gpson';
            },
            function (error) {
                $scope.contentScreen.geolocation = null;
            }
        );
}
*/
    //!!!
    $scope.tryAPIGeolocation = function () {
        
            jQuery.post("https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyC9Pn0GP3LyqmTxUXsOOvtku3775XAgyDA",
                function (success) {
                    $scope.contentScreen.geolocation = {
                        lat: success.location.lat,
                        lon: success.location.lng,
                        formatted_address: (EMAP.Localization.Culture == 'en') ? EMAP.Constants.from_arr[0].formatted_address_en : EMAP.Constants.from_arr[0].formatted_address_ar
                    };

                    $scope.contentScreen.t_route.geolocationClass = 'icon-gpson';
                }
          )
          .fail(function(err) {
              //alert("API Geolocation error! \n\n"+err);
              $scope.contentScreen.geolocation = null;
          });
          
        };

        //$scope.tryAPIGeolocation();
    //!!!


    //Map
    $('.emap-top-menu').popover();

    $('body').on('click', function (e) {
        $('[data-toggle="popover"]').each(function () {
            //the 'is' for buttons that trigger popups
            //the 'has' for icons within a button that triggers a popup
            if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {
                $(this).popover('hide');
            }
        });
    });

    $scope.EMAP = window.EMAP;
    $scope.EMAP.initMap();

    var skipStart_ = $scope.startScreen.getCookie('skipStart_');
    if (GIS.params.eid > 0 || GIS.params.tid > 0 || (skipStart_ && skipStart_ == "true")) {
        $('#rs-start-screen').css('display', 'none');
        search.getEstates($scope.prefix).then(function (res) {
            $scope.contentScreen.estates = res;

            var features = [];
            jQuery.each($scope.contentScreen.estates, function (idx, estate) {

                var feature = new ol.Feature({
                    estateId: estate.Id,
                    geometry: new ol.geom.Point(ol.proj.transform([estate.lon, estate.lat], 'EPSG:4326', 'EPSG:3857'))
                });
                var iconStyle = new ol.style.Style({
                    image: new ol.style.Icon(({
                        anchor: [0.5, 0.5],
                        anchorXUnits: 'fraction',
                        anchorYUnits: 'pixels',
                        opacity: 0.75,
                        src: 'Images/marker.gif'
                    })),
                    text: estate.Id == 2 ? EMAP.createTextStyle("") : EMAP.createTextStyle(estate.EstateName)
                });
                feature.setStyle(iconStyle);
                features.push(feature);
            });
            EMAP.iconsSource.addFeatures(features);

            GIS.State = GIS.Binary.setBit(GIS.State, GIS.BIT_ESTATES_LOADED);
            if (GIS.params.eid > 0 && GIS.Binary.checkBits(GIS.State, [GIS.BIT_ESTATES_LOADED, GIS.BIT_CLASSIFICATIONS_LOADED])) {
                $scope.contentScreen.onEstateSelected(GIS.params.eid);
            }
        });

        search.getClassifications($scope.prefix).then(function (res) {
            var classes = [];
            jQuery.each(res, function (id, item) {
                classes.push(item);
            });

            $scope.contentScreen.classifications = classes;

            GIS.State = GIS.Binary.setBit(GIS.State, GIS.BIT_CLASSIFICATIONS_LOADED);
            if (GIS.params.eid > 0 && GIS.Binary.checkBits(GIS.State, [GIS.BIT_ESTATES_LOADED, GIS.BIT_CLASSIFICATIONS_LOADED])) {
                $scope.contentScreen.onEstateSelected(GIS.params.eid);
            }
        });

        $scope.EMAP.Vector.init();
        $scope.EMAP.Route.init();

        $('#rs-content-screen').css('display', 'block');
        $('.ol-overlaycontainer-stopevent').css('display', 'block');
    } else {

        $('#rs-content-screen').css('display', 'none');
        $('.ol-overlaycontainer-stopevent').css('display', 'none');
        search.getSettings($scope.prefix).then(function (res) {
            $scope.startScreen.settings = res;

            $scope.startScreen.settings.tabContent_1_html = $sce.trustAsHtml(res.tabContent_1);
            $scope.startScreen.settings.tabContent_2_html = $sce.trustAsHtml(res.tabContent_2);
            //$scope.startScreen.settings.tabContent_3_html = $sce.trustAsHtml(res.tabContent_3);
            $scope.startScreen.settings.tabContent_4_html = $sce.trustAsHtml(res.tabContent_4);
            //$scope.startScreen.settings.tabContent_5_html = $sce.trustAsHtml(res.tabContent_5);
            $scope.startScreen.settings.welcome_html = $sce.trustAsHtml(res.welcomeText);

            $('#emap-start-tab-3-selector').tab('show');
            $('.emap-start-screen-content-block').fadeIn('slow');
            //console.log('a');
        });
        $('#rs-start-screen').css('display', 'block');

        $('#rs-start-screen .emap-context-menu').css('display', 'block');
        $('#rs-start-screen .emap-logo').css('display', 'block');
    }


    $scope.EMAP.map.on('singleclick', $scope.contentScreen.onClick);

    var target = EMAP.map.getTarget();
    var jTarget = typeof target === "string" ? $("#" + target) : $(target);
    // change mouse cursor when over marker
    $(EMAP.map.getViewport()).on('mousemove', function (e) {
        var pixel = EMAP.map.getEventPixel(e.originalEvent);
        var hit = EMAP.map.forEachFeatureAtPixel(pixel, function (feature, layer) {
            return true;
        });
        if (hit) {
            jTarget.css("cursor", "pointer");
        } else {
            jTarget.css("cursor", "");
        }
    });

        

    EMAP.prev_zoom = 100;
    EMAP.map.getView().on('change:resolution', function () {
        if (EMAP.iconsLayer && EMAP.iconsLayer.getVisible() && EMAP.iconsSource)
        {

            var zoom = EMAP.map.getView().getZoom();
            var features = EMAP.iconsSource.getFeatures();

            if (zoom >= 9 && EMAP.prev_zoom == zoom - 1)
            {
                for (var i = 0; i < features.length; i++) {
                    var f = features[i];
                    if (f.get('estateId') == 2) {
                        var estateName = "";
                        jQuery.each($scope.contentScreen.estates, function (idx, estate) {
                            if (estate.Id == 2)
                                estateName = estate.EstateName;
                        });

                        var align = 'center';
                        var baseline = 'Alphabetic';
                        var size = '14px';
                        var offsetX = 0;
                        var offsetY = -10;
                        var weight = 500;
                        var rotation = 0;
                        var font = weight + ' ' + size + ' ' + 'Arial';
                        var fillColor = '#fff';
                        var outlineColor = '#000';
                        var outlineWidth = 1;

                        var textStyle = new ol.style.Text({
                            textAlign: align,
                            textBaseline: baseline,
                            font: font,
                            text: estateName,
                            fill: new ol.style.Fill({ color: fillColor }),
                            stroke: new ol.style.Stroke({ color: outlineColor, width: outlineWidth }),
                            offsetX: offsetX,
                            offsetY: offsetY,
                            rotation: rotation
                        });

                        var iconStyle = new ol.style.Style({
                            image: new ol.style.Icon(({
                                anchor: [0.5, 0.5],
                                anchorXUnits: 'fraction',
                                anchorYUnits: 'pixels',
                                opacity: 0.75,
                                src: 'Images/marker.gif'
                            })),
                            text: textStyle
                        });

                        f.setStyle(iconStyle);
                    }
                }
            }
            else if (zoom < 9 && EMAP.prev_zoom == zoom + 1)
            {
                for (var i = 0; i < features.length; i++) {
                    var f = features[i];
                    if (f.get('estateId') == 2) {

                        var align = 'center';
                        var baseline = 'Alphabetic';
                        var size = '14px';
                        var offsetX = 0;
                        var offsetY = -10;
                        var weight = 500;
                        var rotation = 0;
                        var font = weight + ' ' + size + ' ' + 'Arial';
                        var fillColor = '#fff';
                        var outlineColor = '#000';
                        var outlineWidth = 1;

                        var textStyle = ol.style.Text({
                            textAlign: align,
                            textBaseline: baseline,
                            font: font,
                            text: "",
                            fill: new ol.style.Fill({ color: fillColor }),
                            stroke: new ol.style.Stroke({ color: outlineColor, width: outlineWidth }),
                            offsetX: offsetX,
                            offsetY: offsetY,
                            rotation: rotation
                        });

                        var iconStyle = new ol.style.Style({
                            image: new ol.style.Icon(({
                                anchor: [0.5, 0.5],
                                anchorXUnits: 'fraction',
                                anchorYUnits: 'pixels',
                                opacity: 0.75,
                                src: 'Images/marker.gif'
                            })),
                            text: textStyle
                        });

                        f.setStyle(iconStyle);
                    }
                }
            }
            EMAP.prev_zoom = zoom;
        }
    });
}

(function (ang, $, undefined) {

    "use strict";
    angular.module('ss_emap', [
        'emap.places',
        'emap.search',
        'ngAnimate',
        'cgBusy',
        'directive.loading']
        //'angulike']
    )
    .controller('mainController', ['$scope', '$sce', 'search','$timeout', mainController]);
    /*
    .run([
          '$rootScope', function ($rootScope) {
              $rootScope.facebookAppId = '[1664848010456718]';
          }
    ]);
    */

}(window.ang = window.ang || {}, jQuery));

