$(document).ready(function () {

    $('.ol-attribution').css('display', 'none')

    var divHTML = '<div class="ol-unselectable ol-control emap-tools-measure">' +
        '<button id="emap-measure-length" type="button" title="' + EMAP.Localization.Measure_length + '" style="position:relative;">' +
            '<i class="icon-line"></i>' +
        '</button>' +
        '<button id="emap-measure-area" type="button" title="' + EMAP.Localization.Measure_area + '" style="position:relative;">' +
            '<i class="icon-hexagon-polygon"></i>' +
        '</button>' +
        '<button id="emap-map-center" type="button" title="' + EMAP.Localization.Home + '" style="position:relative;">' +
            '<i class="icon-world"></i>' +
        '</button>' +
        '<button id="emap-services-hide" style="background-color:rgba(0, 60, 136, .1)" type="button" title="' + EMAP.Localization.Services_Hide + '" style="position:relative;">' +
            '<i class="icon-eye-close"></i>' +
        '</button>' +
    '</div>';


    
    $('.ol-zoom-in').prop('title', EMAP.Localization.ZoomIn);
    $('.ol-zoom-out').prop('title', EMAP.Localization.ZoomOut);

    jQuery('.ol-zoom.ol-unselectable.ol-control').after(divHTML);

    jQuery('#emap-services-hide').click(function (evt) {
        /*
        if ($('#rs-content-screen').css('display') == 'none') {
            $('#rs-content-screen').css('display', 'block');
            jQuery('#emap-services-hide').css('background-color', 'rgba(0, 60, 136, .1)');
        }
        else {
            jQuery('#emap-services-hide').css('background-color', '#4c6079');
            $('#rs-content-screen').css('display', 'none');
        }
        */
        var scope = angular.element('body').scope();

        if (!scope)
            return;

        if (!EMAP.Layers || EMAP.Layers.length == 0)
            return;

        if (EMAP.Settings.layerId)
        {
            jQuery('#emap-services-hide').css('background-color', 'rgba(0, 60, 136, .1)');
            scope.$apply(function () {
                scope.contentScreen.showLegendPopup = true;
            });

            if (EMAP.Settings.hasCriteria)
            {
                for (var i = 0; i < EMAP.Layers.length; i++) {
                    var layer = EMAP.Layers[i];

                    if (layer.get('isRaster') || layer.get('isIcons'))
                        continue;

                    if (layer.get('hasCriteria') && layer.get('estateID') == EMAP.Settings.layerId && layer.get('title') == EMAP.Settings.criteria)
                        EMAP.Wms.fitToLayerExtent(layer);
                    else
                        layer.setVisible(false);
                }
            }
            else
            {
                for (var i = 0; i < EMAP.Layers.length; i++) {
                    var layer = EMAP.Layers[i];

                    if (layer.get('isRaster') || layer.get('isIcons'))
                        continue;

                    if (layer.get('hasCriteria')) {
                        layer.setVisible(false);
                        continue;
                    }

                    if (layer.get('estateID') == EMAP.Settings.layerId)
                        EMAP.Wms.fitToLayerExtent(layer);
                    else
                        layer.setVisible(false);
                }
            }

            EMAP.Settings.layerId = null;
            EMAP.Settings.criteria = null;
        }
        else
        {
            jQuery('#emap-services-hide').css('background-color', '#4c6079');
            scope.$apply(function () {
                scope.contentScreen.showLegendPopup = false;
            });

            for (var i = 0; i < EMAP.Layers.length; i++) {
                var layer = EMAP.Layers[i];

                if (layer.get('isRaster') || layer.get('isIcons'))
                    continue;


                if (layer.getVisible()) {
                    EMAP.Settings.layerId = layer.get('estateID');
                    EMAP.Settings.criteria = layer.get('title');
                    EMAP.Settings.hasCriteria = layer.get('hasCriteria');

                    layer.setVisible(false);
                }
            }

        }

        //$('.ol-overlaycontainer-stopevent').css('display', 'none');

        //$('#rs-start-screen').css('display', 'block');
        //$('#rs-start-screen .emap-context-menu').css('display', 'block');
        //$('#rs-start-screen .emap-logo').css('display', 'block');
    });
    
    jQuery('#emap-map-center').click(function (evt) {
        var view = EMAP.map.getView();
        view.setCenter([6132795.443472614, 2442274.4548305715]);
        view.setZoom(6);

        EMAP.Wms.showIcons();
    });

    jQuery('#emap-measure-length').click(function (evt) {
        if (EMAP.Measure.currentType == "LineString") {
            if (EMAP.Measure.ON)
                jQuery('#emap-measure-length').css('background-color', 'rgba(0, 60, 136, .1)');
            else
                jQuery('#emap-measure-length').css('background-color', '#4c6079');
        }
        else if (EMAP.Measure.currentType == "Polygon")
        {
            jQuery('#emap-measure-area').css('background-color', 'rgba(0, 60, 136, .1)');
            jQuery('#emap-measure-length').css('background-color', '#4c6079');
        }
        else {
            jQuery('#emap-measure-length').css('background-color', '#4c6079');
        }


        EMAP.Measure.toggle('LineString');
        evt.stopPropagation();
    });
    jQuery('#emap-measure-area').click(function (evt) {
        if (EMAP.Measure.currentType == "Polygon") {
            if (EMAP.Measure.ON)
                jQuery('#emap-measure-area').css('background-color', 'rgba(0, 60, 136, .1)');
            else
                jQuery('#emap-measure-area').css('background-color', '#4c6079');
        }
        else if (EMAP.Measure.currentType == "LineString")
        {
            jQuery('#emap-measure-area').css('background-color', '#4c6079');
            jQuery('#emap-measure-length').css('background-color', 'rgba(0, 60, 136, .1)');
        }
        else {
            jQuery('#emap-measure-area').css('background-color', '#4c6079');
        }

        EMAP.Measure.toggle('Polygon');
        evt.stopPropagation();
    });

    //sidebar tabs
    $('#myTabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show');
    })
});

$(function () {
       /*
    $('.emap-sidebar-toggle-left').on('click', function () {

        $('#emap-sidebar-inner-wrapper').fadeOut('slide', function () {
            $('.mini-submenu-left').fadeIn();
        });
    });
    $('.emap-sidebar-toggle-right').on('click', function () {
        $('#emap-sidebar-inner-wrapper').fadeOut('slide', function () {
            $('.mini-submenu-left').fadeIn();
        });
    });

    $('.mini-submenu-left').on('click', function () {
        var thisEl = $(this);
        $('#emap-sidebar-inner-wrapper').toggle('slide');
        thisEl.hide();
    });

    $('#emap-sidebar-splitter').on('click', function () {
        var top_px = $('#emap-sidebar-wrapper').css('top'),
            top = 0;

        var step = parseInt((window.innerHeight - 50 - 270) / 3);

        if (top_px) {
            top = top_px.split('px');
            top = parseInt(top[0]);

            if (top > (window.innerHeight - 60))
                $('#emap-sidebar-splitter').addClass('up');

            if (top <= 270)
                $('#emap-sidebar-splitter').removeClass('up');

            if ($('#emap-sidebar-splitter').hasClass('up'))
                top -= step;
            else
                top += step;

            top_px = top + 'px';
            $('#emap-sidebar-wrapper').css('top', top_px);

            if (top > (window.innerHeight - 60))
                $('#emap-sidebar-splitter').addClass('up');

            if (top <= 270)
                $('#emap-sidebar-splitter').removeClass('up');

        }
    });
    */
});
