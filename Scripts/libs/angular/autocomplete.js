/*
 * angular-google-places-autocomplete
 *
 * Copyright (c) 2014 "kuhnza" David Kuhn
 * Licensed under the MIT license.
 * https://github.com/kuhnza/angular-google-places-autocomplete/blob/master/LICENSE
 */

 'use strict';

angular.module('emap.places', [])
	/**
	 * DI wrapper around global google places library.
	 *
	 * Note: requires the Google Places API to already be loaded on the page.
	 */
	.factory('emapPlacesApi', ['$window', function ($window) {
        //if (!$window.google) throw 'Global `google` var missing. Did you forget to include the places API script?';

		//return $window.google;
		return {};
	}])

	/**
	 * Autocomplete directive. Use like this:
	 *
	 * <input type="text" g-places-autocomplete ng-model="myScopeVar" />
	 */
	.directive('gPlacesAutocomplete',
        [ '$http', '$parse', '$compile', '$timeout', '$document', 'emapPlacesApi',
        function ($http, $parse, $compile, $timeout, $document, emap) {

            return {
                restrict: 'A',
                require: '^ngModel',
                scope: {
                    model: '=ngModel',
                    options: '=?',
                    forceSelection: '=?',
                    customPlaces: '=?'
                },
                controller: ['$scope', function ($scope) {}],
                link: function ($scope, element, attrs, controller) {
                    var keymap = {
                            tab: 9,
                            enter: 13,
                            esc: 27,
                            up: 38,
                            down: 40
                        },
                        hotkeys = [keymap.tab, keymap.enter, keymap.esc, keymap.up, keymap.down];

                        $scope.mode = attrs.customPlaces;
                        //console.log(attrs.customPlaces);
						//!!!
                        //autocompleteService = new google.maps.places.AutocompleteService(),
                        //placesService = new google.maps.places.PlacesService(element[0]);
						//!!!

                    (function init() {
                        $scope.query = '';
                        $scope.predictions = [];
                        $scope.input = element;
                        $scope.options = $scope.options || {};

                        initAutocompleteDrawer();
                        initEvents();
                        initNgModelController();
                    }());

                    function initEvents() {
                        element.bind('focus', onFocus);
                        element.bind('keydown', onKeydown);
                        element.bind('blur', onBlur);
                        element.bind('submit', onBlur);

                        $scope.$watch('selected', select);
                    }

                    function initAutocompleteDrawer() {
                        //console.log('initAutocompleteDrawer');

                        // Drawer element used to display predictions
                        var drawerElement = angular.element('<div g-places-autocomplete-drawer></div>'),
                            body = angular.element($document[0].body),
                            $drawer;

                        drawerElement.attr({
                            input: 'input',
                            query: 'query',
                            predictions: 'predictions',
                            active: 'active',
                            selected: 'selected'
                        });

                        $drawer = $compile(drawerElement)($scope);
                        body.append($drawer);  // Append to DOM
                    }
                    function initNgModelController() {
                        //console.log('initNgModelController');

                        controller.$parsers.push(parse);
                        controller.$formatters.push(format);
                        controller.$render = render;
                    }
                    function onFocus(event) {
                        //$("#emap-debug").html($("#emap-debug").html() + '<p>a:focus</p>');
                        //console.log('focus');
                        //$("#emap-debug").html($("#emap-debug").html() + ' focus<br>');

                        if ($scope.mode == "tenants") {
                            var toggle_prefix = window.innerWidth >= 768 ? "" : "-mobile";
                            var isActive = $('#emap-estates-dropdown' + toggle_prefix).hasClass('open');
                            var isFilled = $("#emap-tenants-main-search" + toggle_prefix).val() && $("#emap-tenants-main-search" + toggle_prefix).val().length > 0;
                            if (!isActive && !isFilled) {
                                $('#emap-estates-dropdown' + toggle_prefix + ' button').dropdown('emap_toggle');
                            }
                        } else if ($scope.mode == "tenantsForEstate") {
                            var isActive = $('#emap-tenants-dropdown').hasClass('open');
                            var isFilled = $("#emap-tenants-search").val() && $("#emap-tenants-search").val().length > 0;
                            if (!isActive && !isFilled) {
                                $('#emap-tenants-dropdown button').dropdown('emap_toggle');
                            }
                        }
                        else if ($scope.mode == "t_routeFrom")
                        {
                            var predictions = [],
                                place, match, i;

                            var data = EMAP.Constants.from_arr;
                            $scope.customPlaces = data;

                            for (i = 0; i < $scope.customPlaces.length; i++) {
                                place = $scope.customPlaces[i];
                                place.formatted_address = (EMAP.Localization.Culture == 'en') ? place.formatted_address_en : place.formatted_address_ar,

                                predictions.push({
                                    is_custom: true,
                                    custom_prediction_label: "",
                                    description: place.formatted_address,
                                    place: place,
                                    matched_substrings: [{length: 1, offset: 0}],
                                    terms: [{offset: 0, value: place.formatted_address}]
                                });
                            }

                            $scope.predictions.push.apply($scope.predictions, predictions);

                            if ($scope.predictions.length > 15) {
                                $scope.predictions.length = 15;  // trim predictions down to size
                            }

                            $scope.$digest();
                        }
                    }
                    function onKeydown(event) {
                        //console.log('onKeydown');

                            if ($scope.predictions.length === 0 || indexOf(hotkeys, event.which) === -1) {
                                return;
                            }

                            event.preventDefault();

                            if (event.which === keymap.down) {
                                $scope.active = ($scope.active + 1) % $scope.predictions.length;
                                $scope.$digest();
                            } else if (event.which === keymap.up) {
                                $scope.active = ($scope.active ? $scope.active : $scope.predictions.length) - 1;
                                $scope.$digest();
                            } else if (event.which === 13 || event.which === 9) {
                                if ($scope.forceSelection) {
                                    $scope.active = ($scope.active === -1) ? 0 : $scope.active;
                                }

                                $scope.$apply(function () {
                                    $scope.selected = $scope.active;

                                    if ($scope.selected === -1) {
                                        clearPredictions();
                                    }
                                });
                            } else if (event.which === 27) {
                                event.stopPropagation();
                                clearPredictions();
                                $scope.$digest();
                            }
                        //}
                    }

                    function onBlur(event) {
                        //if (!EMAP.IsOwnBlur) {
                        //console.log('blur');
                        //$("#emap-debug").html($("#emap-debug").html() + '<p>a:blur</p>');
                        //$("#emap-debug").html($("#emap-debug").html() + ' blur<br>');
                        //var toggle_prefix = window.innerWidth >= 768 ? "" : "-mobile";
                        //var isActive = $('#emap-estates-dropdown' + toggle_prefix).hasClass('open');
                        //if ($scope.mode == "tenants" && isActive) {
                            //$('#emap-estates-dropdown' + toggle_prefix + ' button').dropdown('emap_toggle');
                        //}

                            if ($scope.predictions.length === 0) {
                                if ($scope.forceSelection) {
                                    var phase = $scope.$root.$$phase;
                                    var fn = function () {
                                        $scope.model = '';
                                    }
                                    if (phase == '$apply' || phase == '$digest') {
                                        fn();
                                    } else {
                                        $scope.$apply(fn);
                                    }
                                }
                                return;
                            }

                            if ($scope.forceSelection) {
                                $scope.selected = ($scope.selected === -1) ? 0 : $scope.selected;
                            }

                            $scope.$digest();

                            $scope.$apply(function () {
                                if (!$scope.selected || $scope.selected === -1) {
                                    clearPredictions();
                                }
                            });
                        //}
                    }

                    function select() {
                        //console.log('select');

                        var prediction = $scope.predictions[$scope.selected];
                        if (!prediction) return;

                        if (prediction.is_custom) {
                            $scope.model = prediction.place;
                            $scope.$emit('g-places-autocomplete:select.' + $scope.mode, { place: prediction.place });
                        }

                        clearPredictions();
                    }

                    function parse(viewValue) {
                        var request;

                        //!!!
                        if (!(viewValue && isString(viewValue))) {
                            clearPredictions();

                            if ($scope.mode == "tenants") {
                                var toggle_prefix = window.innerWidth >= 768 ? "" : "-mobile";
                                var isActive = $('#emap-estates-dropdown' + toggle_prefix).hasClass('open');
                                if (!isActive) {
                                    $('#emap-estates-dropdown' + toggle_prefix + ' button').dropdown('emap_toggle');
                                }
                            } else if ($scope.mode == "tenantsForEstate") {
                                var isActive = $('#emap-tenants-dropdown').hasClass('open');
                                if (!isActive) {
                                    $('#emap-tenants-dropdown button').dropdown('emap_toggle');
                                }
                            }

                            return viewValue;
                        }
                        else
                        {
                            if ($scope.mode == "tenants") {
                                var toggle_prefix = window.innerWidth >= 768 ? "" : "-mobile";
                                var isActive = $('#emap-estates-dropdown' + toggle_prefix).hasClass('open');
                                if (isActive) {
                                    $('#emap-estates-dropdown' + toggle_prefix + ' button').dropdown('emap_toggle');
                                }
                            } else if ($scope.mode == "tenantsForEstate") {
                                var isActive = $('#emap-tenants-dropdown').hasClass('open');
                                if (isActive) {
                                    $('#emap-tenants-dropdown button').dropdown('emap_toggle');
                                }
                            }
                        }
                        //!!!



                        $scope.query = viewValue;

						clearPredictions();
						getCustomPlacePredictions($scope.query);

                        return viewValue;
                    }

                    function format(modelValue) {
                        var viewValue = "";

                        if (isString(modelValue)) {
                            viewValue = modelValue;
                        } else if (isObject(modelValue)) {
                            viewValue = modelValue.formatted_address;
                        }

                        return viewValue;
                    }

                    function render() {
                        return element.val(controller.$viewValue);
                    }

                    function clearPredictions() {
                        $scope.active = -1;
                        $scope.selected = -1;
                        $scope.predictions.length = 0;
                    }

                    function getCustomPlacePredictions(query) {

                        //return res;
						var predictions = [],
                            place, match, i;

						switch ($scope.mode) {
						    case "tenants":
						    //case "routeTo":
						    //case "routeFromSwap":
						    //case "t_routeTo":
						    //case "t_routeFromSwap":
						        $http({
						            method: "POST",
						            url: "EmapWebService.asmx/getArticlesBySearchString",
						            data: "{'searchString':'" + query + "','prefix':'" + EMAP.Localization.Culture + "'}",
						            contentType: "application/json; charset=utf-8",
						            dataType: "json"
						        }).then(function (resp) {
						            var data = JSON.parse(resp.data.d);
						            $scope.customPlaces = data;

						            for (i = 0; i < $scope.customPlaces.length; i++) {
						                place = $scope.customPlaces[i];

						                match = getCustomPlaceMatches(query, place);
						                if (match.matched_substrings.length > 0) {
						                    predictions.push({
						                        is_custom: true,
						                        custom_prediction_label: "", //place.IsEstate ? "(estate)" : "(tenant)",
						                        description: place.formatted_address,
						                        place: place,
						                        matched_substrings: match.matched_substrings,
						                        terms: match.terms
						                    });
						                }
						            }

						            $scope.predictions.push.apply($scope.predictions, predictions);

						            if ($scope.predictions.length > 15) {
						                $scope.predictions.length = 15;  // trim predictions down to size
						            }
						        });
						        break;
						    case "tenantsForEstate":
						        var app_scope = angular.element('body').scope();
						        if (!app_scope)
						            return;

						        $http({
						            method: "POST",
						            url: "EmapWebService.asmx/getArticlesBySearchStringAndEstateId",
						            data: "{'searchString':'" + query + "', 'estateId':'" + app_scope.contentScreen.selected_estate.Id + "','prefix':'" + EMAP.Localization.Culture + "'}",
						            contentType: "application/json; charset=utf-8",
						            dataType: "json"
						        }).then(function (resp) {
						            var data = JSON.parse(resp.data.d);
						            $scope.customPlaces = data;

						            for (i = 0; i < $scope.customPlaces.length; i++) {
						                place = $scope.customPlaces[i];

						                match = getCustomPlaceMatches(query, place);
						                if (match.matched_substrings.length > 0) {
						                    predictions.push({
						                        is_custom: true,
						                        custom_prediction_label: "", //place.IsEstate ? "(estate)" : "(tenant)",
						                        description: place.formatted_address,
						                        place: place,
						                        matched_substrings: match.matched_substrings,
						                        terms: match.terms
						                    });
						                }
						            }

						            $scope.predictions.push.apply($scope.predictions, predictions);

						            if ($scope.predictions.length > 15) {
						                $scope.predictions.length = 15;  // trim predictions down to size
						            }
						        });
						        break;
						    //case "routeToSwap":
						    //case "routeFrom":
                            //case "t_routeToSwap":
						    case "t_routeFrom":
						        var data = EMAP.Constants.from_arr;
						        $scope.customPlaces = data;

						        for (i = 0; i < $scope.customPlaces.length; i++) {
						            place = $scope.customPlaces[i];
						            place.formatted_address = (EMAP.Localization.Culture == 'en') ? place.formatted_address_en : place.formatted_address_ar,

						            match = getCustomPlaceMatches(query, place);
						            if (match.matched_substrings.length > 0) {
						                predictions.push({
						                    is_custom: true,
						                    custom_prediction_label: "",
						                    description: place.formatted_address,
						                    place: place,
						                    matched_substrings: match.matched_substrings,
						                    terms: match.terms
						                });
						            }
						        }

						        $scope.predictions.push.apply($scope.predictions, predictions);

						        if ($scope.predictions.length > 15) {
						            $scope.predictions.length = 15;  // trim predictions down to size
						        }
						        break;
						    case "classificationCountry":
						        var app_scope = angular.element('body').scope();
						        if (!app_scope)
						            return;

						        $http({
						            method: "POST",
						            url: "EmapWebService.asmx/getCountriesBySearchStringAndEstateId",
						            data: "{'searchString':'" + query + "', 'estateId':'" + app_scope.contentScreen.selected_estate.Id + "','prefix':'" + EMAP.Localization.Culture + "'}",
						            contentType: "application/json; charset=utf-8",
						            dataType: "json"
						        }).then(function (resp) {
						            var data = JSON.parse(resp.data.d);
						            $scope.customPlaces = data;

						            for (i = 0; i < $scope.customPlaces.length; i++) {
						                place = $scope.customPlaces[i];

						                match = getCustomPlaceMatches(query, place);
						                if (match.matched_substrings.length > 0) {
						                    predictions.push({
						                        is_custom: true,
						                        custom_prediction_label: "", //place.IsEstate ? "(estate)" : "(tenant)",
						                        description: place.formatted_address,
						                        place: place,
						                        matched_substrings: match.matched_substrings,
						                        terms: match.terms
						                    });
						                }
						            }

						            $scope.predictions.push.apply($scope.predictions, predictions);

						            if ($scope.predictions.length > 15) {
						                $scope.predictions.length = 15;  // trim predictions down to size
						            }
						        });
						        break;
						    case "classificationProductType":
						        var app_scope = angular.element('body').scope();
						        if (!app_scope)
						            return;

						        $http({
						            method: "POST",
						            url: "EmapWebService.asmx/getProductTypesBySearchStringAndEstateId",
						            data: "{'searchString':'" + query + "', 'estateId':'" + app_scope.contentScreen.selected_estate.Id + "','prefix':'" + EMAP.Localization.Culture + "'}",
						            contentType: "application/json; charset=utf-8",
						            dataType: "json"
						        }).then(function (resp) {
						            var data = JSON.parse(resp.data.d);
						            $scope.customPlaces = data;

						            for (i = 0; i < $scope.customPlaces.length; i++) {
						                place = $scope.customPlaces[i];

						                match = getCustomPlaceMatches(query, place);
						                if (match.matched_substrings.length > 0) {
						                    predictions.push({
						                        is_custom: true,
						                        custom_prediction_label: "", //place.IsEstate ? "(estate)" : "(tenant)",
						                        description: place.formatted_address,
						                        place: place,
						                        matched_substrings: match.matched_substrings,
						                        terms: match.terms
						                    });
						                }
						            }

						            $scope.predictions.push.apply($scope.predictions, predictions);

						            if ($scope.predictions.length > 15) {
						                $scope.predictions.length = 15;  // trim predictions down to size
						            }
						        });
						        break;
						}
						//return predictions;
                    }

                    function getCustomPlaceMatches(query, place) {
                        var q = query + '',  // make a copy so we don't interfere with subsequent matches
                            terms = [],
                            matched_substrings = [],
                            fragment,
                            termFragments,
                            i;

                        //termFragments = place.formatted_address.split(',');
                        //for (i = 0; i < termFragments.length; i++) {
                            //fragment = termFragments[i].trim();
                            fragment = place.formatted_address;

                            if (q.length > 0) {
                                if (fragment.length >= q.length) {
                                    //!!!
                                    if (fragment.toLowerCase().indexOf(q) >= 0) {
                                        matched_substrings.push({ length: q.length, offset: fragment.toLowerCase().indexOf(q) });
                                    }
                                    //!!!

                                    //if (startsWith(fragment, q)) {
                                    //    matched_substrings.push({ length: q.length, offset: i });
                                    //}
                                    q = '';  // no more matching to do
                                } else {
                                    //!!!
                                    if (q.indexOf(fragment.toLowerCase()) >= 0) {
                                        matched_substrings.push({ length: fragment.length, offset: q.indexOf(fragment.toLowerCase()) });
                                        q = q.replace(fragment.toLowerCase(), '').trim();
                                    //!!!
                                    //if (startsWith(q, fragment)) {
                                    //    matched_substrings.push({ length: fragment.length, offset: i });
                                    //    q = q.replace(fragment, '').trim();
                                    } else {
                                        q = '';  // no more matching to do
                                    }
                                }
                            }

                            terms.push({
                                value: fragment,
                                //offset: place.formatted_address.indexOf(fragment)
                                offset: place.formatted_address.indexOf(q)
                            });
                        //}

                        return {
                            matched_substrings: matched_substrings,
                            terms: terms
                        };
                    }

                    function isString(val) {
                        return Object.prototype.toString.call(val) == '[object String]';
                    }

                    function isObject(val) {
                        return Object.prototype.toString.call(val) == '[object Object]';
                    }

                    function indexOf(array, item) {
                        var i, length;

                        if (array == null) return -1;

                        length = array.length;
                        for (i = 0; i < length; i++) {
                            if (array[i] === item) return i;
                        }
                        return -1;
                    }

                    function startsWith(string1, string2) {
                        return toLower(string1).lastIndexOf(toLower(string2), 0) === 0;
                    }

                    function toLower(string) {
                        return (string == null) ? "" : string.toLowerCase();
                    }
                }
            }
        }
    ])

    .directive('gPlacesAutocompleteDrawer', ['$window', '$document', function ($window, $document) {

        var lang_style = (EMAP.Localization.Culture == 'en') ? '' : 'text-align:right';

        var TEMPLATE = [
            '<div class="pac-container" ng-if="isOpen()" ng-style="{top: position.top+\'px\', left: position.left+\'px\', width: position.width+\'px\'}" style="display: block;z-index:1000111' + lang_style + '" role="listbox" aria-hidden="{{!isOpen()}}">',
            '  <div class="pac-item" style="' + lang_style + '" g-places-autocomplete-prediction index="$index" prediction="prediction" query="query"',
            '       ng-repeat="prediction in predictions track by $index" ng-class="{\'pac-item-selected\': isActive($index) }"',
            '       ng-mouseenter="selectActive($index)" ng-click="selectPrediction($index)" role="option" id="{{prediction.id}}">',
            '  </div>',
            '</div>'

        ];

        return {
            restrict: 'A',
            scope:{
                input: '=',
                query: '=',
                predictions: '=',
                active: '=',
                selected: '='
            },
            template: TEMPLATE.join(''),
            link: function ($scope, element) {
                element.bind('mousedown', function (event) {
                    event.preventDefault();  // prevent blur event from firing when clicking selection
                });

                $scope.isOpen = function () {
                    return $scope.predictions.length > 0;
                };

                $scope.isActive = function (index) {
                    return $scope.active === index;
                };

                $scope.selectActive = function (index) {
                    $scope.active = index;
                };

                $scope.selectPrediction = function (index) {
                    $scope.selected = index;
                };

                $scope.$watch('predictions', function () {
                    $scope.position = getDrawerPosition($scope.input);
                }, true);

                function getDrawerPosition(element) {
                    var domEl = element[0],
                        rect = domEl.getBoundingClientRect(),
                        docEl = $document[0].documentElement,
                        body = $document[0].body,
                        scrollTop = $window.pageYOffset || docEl.scrollTop || body.scrollTop,
                        scrollLeft = $window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

                    return {
                        width: rect.width,
                        height: rect.height,
                        top: rect.top + rect.height + scrollTop,
                        left: rect.left + scrollLeft
                    };
                }
            }
        }
    }])

    .directive('gPlacesAutocompletePrediction', [function () {
        var TEMPLATE = [
            '<span class="pac-icon pac-icon-marker"></span>',
            '<span class="pac-item-query" ng-bind-html="prediction | highlightMatched"></span>',
            '<span ng-repeat="term in prediction.terms | unmatchedTermsOnly:prediction">{{term.value | trailingComma:!$last}}&nbsp;</span>',
            '<span class="custom-prediction-label" ng-if="prediction.is_custom">&nbsp;{{prediction.custom_prediction_label}}</span>'
        ];

        return {
            restrict: 'A',
            scope:{
                index:'=',
                prediction:'=',
                query:'='
            },
            template: TEMPLATE.join('')
        }
    }])

    .filter('highlightMatched', ['$sce', function ($sce) {
        return function (prediction) {
            var matchedPortion = '',
                pre_unmatchedPortion = '',
                post_unmatchedPortion = '',
                matched,
				term;

            if (prediction.matched_substrings.length > 0 && prediction.terms.length > 0) {
                matched = prediction.matched_substrings[0];
				term = prediction.terms[0];
                //matchedPortion = prediction.terms[0].value.substr(matched.offset, matched.length); //matched.offcet всегда 0
                //unmatchedPortion = prediction.terms[0].value.substr(matched.offset + matched.length);

				//console.log(prediction.matched_substrings);
				if (matched.offset > 0) {
				    pre_unmatchedPortion = term.value.substr(0, matched.offset);
				    matchedPortion = term.value.substr(matched.offset, matched.length);
				    post_unmatchedPortion = term.value.substr(matched.offset + matched.length);

				}
				else if (matched.offset == 0)
				{
				    pre_unmatchedPortion = '';
				    matchedPortion = term.value.substr(matched.offset, matched.length);
				    post_unmatchedPortion = term.value.substr(matched.offset + matched.length);
				}
            }

            return $sce.trustAsHtml(pre_unmatchedPortion + '<span class="pac-matched">' + matchedPortion + '</span>' + post_unmatchedPortion);
            //return $sce.trustAsHtml(pre_unmatchedPortion);
        }
    }])

    .filter('unmatchedTermsOnly', [function () {
        return function (terms, prediction) {
            var i, term, filtered = [];

            for (i = 0; i < terms.length; i++) {
                term = terms[i];
                if (prediction.matched_substrings.length > 0 && term.offset > prediction.matched_substrings[0].length) {
                    filtered.push(term);
                }
            }

            return filtered;
        }
    }])

    .filter('trailingComma', [function () {
        return function (input, condition) {
            return (condition) ? input + ',' : input;
        }
    }]);


