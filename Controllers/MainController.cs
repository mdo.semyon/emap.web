﻿using Emap.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Emap.Web.Controllers
{
    public class MainController : Controller
    {
        static EmapMeta model = new EmapMeta();

        public ActionResult Index()
        {
            ViewBag.Title = "Peie Map.";


            model.Settings = new Settings();
            model.Settings.liveChatLicense = WebConfigurationManager.AppSettings["liveChatLicense"].ToString();
            model.Settings.workspace = WebConfigurationManager.AppSettings["workspace"].ToString();
            model.Settings.wmsURL = WebConfigurationManager.AppSettings["wmsURL"].ToString();
            model.Settings.bingKey = WebConfigurationManager.AppSettings["bingKey"].ToString();
            model.Settings.companyProfileURL = WebConfigurationManager.AppSettings["companyProfileURL"].ToString();
            model.Settings.map_mode = WebConfigurationManager.AppSettings["map_mode"].ToString();
            model.Settings.map_type = WebConfigurationManager.AppSettings["map_type"].ToString();

            //!!! Meta
            model.Meta = new Meta();
            model.Meta.en_title = WebConfigurationManager.AppSettings["en.title"].ToString();
            model.Meta.en_description = WebConfigurationManager.AppSettings["en.description"].ToString();
            model.Meta.en_keywords = WebConfigurationManager.AppSettings["en.keywords"].ToString();

            model.Meta.ar_title = WebConfigurationManager.AppSettings["ar.title"].ToString();
            model.Meta.ar_description = WebConfigurationManager.AppSettings["ar.description"].ToString();
            model.Meta.ar_keywords = WebConfigurationManager.AppSettings["ar.keywords"].ToString();

            //!!! Facebook
            model.Facebook = new Facebook();
            model.Facebook.en_name = WebConfigurationManager.AppSettings["en.facebook.name"].ToString();
            model.Facebook.en_description = WebConfigurationManager.AppSettings["en.facebook.description"].ToString();
            model.Facebook.ar_name = WebConfigurationManager.AppSettings["ar.facebook.name"].ToString();
            model.Facebook.ar_description = WebConfigurationManager.AppSettings["ar.facebook.description"].ToString();
            model.Facebook.picture = WebConfigurationManager.AppSettings["facebook.picture"].ToString();

            //!!! twitter
            model.Twitter = new Twitter();
            model.Twitter.en_text = WebConfigurationManager.AppSettings["en.twitter.text"].ToString();
            model.Twitter.ar_text = WebConfigurationManager.AppSettings["ar.twitter.text"].ToString();
            //!!!
            
            return View(model);
        }

        public ActionResult ChangeCulture(string lang)
        {
            var langCookie = new HttpCookie("lang", lang) { HttpOnly = true };
            Response.AppendCookie(langCookie);
            return RedirectToAction("Index", "Main", new { culture = lang });
        }
    }
}
